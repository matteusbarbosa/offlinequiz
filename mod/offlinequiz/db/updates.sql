    ALTER TABLE `offlinequiz_queue`
	ADD COLUMN `type` ENUM('essay','multi') NULL DEFAULT NULL AFTER `status`;

    ALTER TABLE `offlinequiz_queue`
	ADD COLUMN `studentid` BIGINT(10) NULL DEFAULT NULL AFTER `type`;

	    ALTER TABLE `offlinequiz_queue`
	ADD COLUMN `grade_existent_action` TINYINT(1) NULL DEFAULT NULL;

CREATE TABLE `offlinequiz_grade_essay` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`queueid` BIGINT(10) NULL DEFAULT NULL,
	`questionid` BIGINT(10) NULL DEFAULT NULL,
	`offlinequizid` BIGINT(10) NOT NULL DEFAULT '0',
	`feedback` TEXT NULL,
	`userid` BIGINT(10) NOT NULL DEFAULT '0',
	`rawgrade` DECIMAL(10,5) NOT NULL DEFAULT '0.00000',
	`timecreated` INT(10) NOT NULL DEFAULT '0',
	`timeupdated` INT(10) NULL DEFAULT NULL,
	`timefinished` INT(10) NULL DEFAULT NULL,
	`grade_existent_action` TINYINT(1) NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=MyISAM
AUTO_INCREMENT=7
;

ALTER TABLE `offlinequiz_grade_essay` COMMENT = 'Armazenamento temporário das notas enviadas na avaliação';


CREATE TABLE `offlinequiz_scanned_essay_pages` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`questionid` INT(11) NULL DEFAULT NULL,
	`userid` INT(11) NULL DEFAULT NULL,
	`answer_link` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`offlinequizid` INT(11) NULL DEFAULT NULL,
	`timecreated` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=MyISAM
;
