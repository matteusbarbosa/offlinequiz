YUI.add('moodle-mod_offlinequiz-questionchooser', function (Y, NAME) {

    var CSS = {
        ADDNEWQUESTIONBUTTONS: '.menu [data-action="addquestion"]',
        CREATENEWQUESTION: 'div.createnewquestion',
        CHOOSERDIALOGUE: 'div.chooserdialoguebody',
        CHOOSERHEADER: 'div.choosertitle'
    };
    
    /**
     * The questionchooser class  is responsible for instantiating and displaying the question chooser
     * when viewing a offlinequiz in editing mode.
     *
     * @class questionchooser
     * @constructor
     * @protected
     * @extends M.core.chooserdialogue
     */
    var QUESTIONCHOOSER = function() {
        QUESTIONCHOOSER.superclass.constructor.apply(this, arguments);
    };
    
    Y.extend(QUESTIONCHOOSER, M.core.chooserdialogue, {
    
        hiddenRadioValue: null,
    
        initializer: function() {
            Y.one('body').delegate('click', this.display_dialogue, CSS.ADDNEWQUESTIONBUTTONS, this);
        },
    
        prepare_chooser: function() {
            if (this.panel) {
                return;
            }
    
            // Ensure that we're showing the JS version of the chooser.
            Y.one(Y.config.doc.body).addClass('jschooser');
    
            // Set Default options
            var paramkey,
                params = {
                    bodyContent: this.bodycontent.get('innerHTML'),
                    headerContent: this.headercontent.get('innerHTML'),
                    width: '540px',
                    draggable: true,
                    visible: false, // Hide by default
                    zindex: 100, // Display in front of other items
                    modal: true, // This dialogue should be modal.
                    shim: true,
                    closeButtonTitle: this.get('closeButtonTitle'),
                    focusOnPreviousTargetAfterHide: true,
                    render: false,
                    extraClasses: this._getClassNames()
                };
    
            // Override with additional options
            for (paramkey in this.instanceconfig) {
              params[paramkey] = this.instanceconfig[paramkey];
            }
    
            // Create the panel
            this.panel = new M.core.dialogue(params);
    
            // Remove the template for the chooser
            this.bodycontent.remove();
            this.headercontent.remove();
    
            // Hide and then render the panel
            this.panel.hide();
            this.panel.render();
    
            // Set useful links.
            this.container = this.panel.get('boundingBox').one('.choosercontainer');
            this.options = this.container.all('.option input[type=radio]');
    
            console.log(this.options);
    
            // The hidden form element we use when submitting.
            this.hiddenRadioValue = Y.Node.create('<input type="hidden" name="qtype" value="">');
            this.container.one('form').appendChild(this.hiddenRadioValue);
    
    
            // Add the chooserdialogue class to the container for styling
            this.panel.get('boundingBox').addClass('chooserdialogue');
        },
    
    
        check_options: function() {
    
            // Check which options are set, and change the parent class
            // to show/hide help as required
            this.options.each(function(thisoption) {
                
                //skip remove elements
                if(thisoption.get('parentNode') === null){
                    return;
                }
        
                var optiondiv = thisoption.get('parentNode').get('parentNode');
                if (thisoption.get('checked')) {
                    console.log(thisoption);
                    optiondiv.addClass('selected');
    
                    // Trigger any events for this option
                    this.option_selected(thisoption);
    
                    // Ensure that the form may be submitted
                    this.submitbutton.removeAttribute('disabled');
    
                    // Ensure that the radio remains focus so that keyboard navigation is still possible
                    thisoption.focus();
                } else {
                    optiondiv.removeClass('selected');
                }
            }, this);
        },
    
        option_selected: function(e) {
            // Set a hidden input field with the value and name of the radio button.  When we submit the form, we
            // disable the radios to prevent duplicate submission. This has the result however that the value is never
            // submitted so we set this value to a hidden field instead
            this.hiddenRadioValue.setAttrs({
                value: e.get('value'),
                name: e.get('name')
            });
        },
    
    
        display_dialogue: function(e) {
            e.preventDefault();
            var dialogue = Y.one(CSS.CREATENEWQUESTION + ' ' + CSS.CHOOSERDIALOGUE),
                header = Y.one(CSS.CREATENEWQUESTION + ' ' + CSS.CHOOSERHEADER);
    
            if (this.container === null) {
                // Setup the dialogue, and then prepare the chooser if it's not already been set up.
                this.setup_chooser_dialogue(dialogue, header, {});
                this.prepare_chooser();
            }
    
            // Update all of the hidden fields within the questionbank form.
            var parameters = Y.QueryString.parse(e.currentTarget.get('search').substring(1));
            var form = this.container.one('form');
            this.parameters_to_hidden_input(parameters, form, 'returnurl');
            this.parameters_to_hidden_input(parameters, form, 'cmid');
            this.parameters_to_hidden_input(parameters, form, 'category');
            this.parameters_to_hidden_input(parameters, form, 'addonpage');
            this.parameters_to_hidden_input(parameters, form, 'appendqnumstring');
    
            // Display the chooser dialogue.
            this.display_chooser(e);
            
    
            var nodes = Y.all('#chooseform input[type=radio]')._nodes;
            for(i = 0; i < nodes.length; i++) {
                if (nodes[i].id != 'item_qtype_multichoiceset' &&
                    nodes[i].id != 'item_qtype_multichoice' &&
                    nodes[i].id != 'item_qtype_description' &&
                    nodes[i].id != 'item_qtype_essay' ) {
                        //remove os tipos de questão não utilizados
                    nodes[i].remove();
                }
            }
    
    
            var options = Y.all('.option')._nodes;
            let qtype = '';
            for(i = 0; i < options.length; i++) {
                qtype = options[i].firstElementChild.getAttribute('for');
                if (qtype != 'item_qtype_multichoiceset' &&
                qtype != 'item_qtype_multichoice' &&
                qtype != 'item_qtype_description' &&
                qtype != 'item_qtype_essay' ) {
                    options[i].remove();
                }
            }
    
    
        },
    
          /**
          * Display the module chooser
          *
          * @method display_chooser
          * @param {EventFacade} e Triggering Event
          */
           display_chooser: function(e) {
            var bb, dialogue, thisevent;
            this.prepare_chooser();
    
            // Stop the default event actions before we proceed
            e.preventDefault();
    
            bb = this.panel.get('boundingBox');
            dialogue = this.container.one('.alloptions');
    
            // This will detect a change in orientation and retrigger centering
            thisevent = Y.one('document').on('orientationchange', function() {
                this.center_dialogue(dialogue);
            }, this);
            this.listenevents.push(thisevent);
    
            // Detect window resizes (most browsers)
            thisevent = Y.one('window').on('resize', function() {
                this.center_dialogue(dialogue);
            }, this);
            this.listenevents.push(thisevent);
    
            // These will trigger a check_options call to display the correct help
            thisevent = this.container.on('click', this.check_options, this);
            this.listenevents.push(thisevent);
            thisevent = this.container.on('key_up', this.check_options, this);
            this.listenevents.push(thisevent);
            thisevent = this.container.on('dblclick', function(e) {
                if (e.target.ancestor('div.option')) {
                    this.check_options();
    
                    // Prevent duplicate submissions
                    this.submitbutton.setAttribute('disabled', 'disabled');
                    this.options.setAttribute('disabled', 'disabled');
                    this.cancel_listenevents();
    
                    this.container.one('form').submit();
                }
            }, this);
            this.listenevents.push(thisevent);
    
            this.container.one('form').on('submit', function() {
                // Prevent duplicate submissions on submit
                this.submitbutton.setAttribute('disabled', 'disabled');
                this.options.setAttribute('disabled', 'disabled');
                this.cancel_listenevents();
            }, this);
    
            // Hook onto the cancel button to hide the form
            thisevent = this.container.one('.addcancel').on('click', this.cancel_popup, this);
            this.listenevents.push(thisevent);
    
            // Hide will be managed by cancel_popup after restoring the body overflow
            thisevent = bb.one('button.closebutton').on('click', this.cancel_popup, this);
            this.listenevents.push(thisevent);
    
            // Grab global keyup events and handle them
            thisevent = Y.one('document').on('keydown', this.handle_key_press, this);
            this.listenevents.push(thisevent);
    
            // Add references to various elements we adjust
            this.submitbutton = this.container.one('.submitbutton');
    
            // Disable the submit element until the user makes a selection
            this.submitbutton.set('disabled', 'true');
    
            // Ensure that the options are shown
            this.options.removeAttribute('disabled');
    
            // Display the panel
            this.panel.show(e);
    
            // Re-centre the dialogue after we've shown it.
            this.center_dialogue(dialogue);
    
            // Finally, focus the first radio element - this enables form selection via the keyboard
            this.container.one('.option input[type=radio]').focus();
    
            // Trigger check_options to set the initial jumpurl
            this.check_options();
        },
    
    
        parameters_to_hidden_input: function(parameters, form, name) {
            var value;
            if (parameters.hasOwnProperty(name)) {
                value = parameters[name];
            } else {
                value = '';
            }
            var input = form.one('input[name=' + name + ']');
            if (!input) {
                input = form.appendChild('<input type="hidden">');
                input.set('name', name);
            }
            input.set('value', value);
        }
    }, {
        NAME: 'mod_offlinequiz-questionchooser'
    });
    
    M.mod_offlinequiz = M.mod_offlinequiz || {};
    M.mod_offlinequiz.init_questionchooser = function() {
        M.mod_offlinequiz.question_chooser = new QUESTIONCHOOSER({});
        return M.mod_offlinequiz.question_chooser;
    };
    
    
    }, '@VERSION@', {"requires": ["moodle-core-chooserdialogue", "moodle-mod_offlinequiz-util", "querystring-parse"]});
    