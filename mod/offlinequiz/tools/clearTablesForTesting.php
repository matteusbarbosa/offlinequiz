<?php


define('CLI_SCRIPT', true);

require_once(dirname(__FILE__) . '/../../../config.php');


$DB->execute("TRUNCATE TABLE offlinequiz_grade_essay");
$DB->execute("TRUNCATE TABLE offlinequiz_scanned_pages");
$DB->execute("TRUNCATE TABLE offlinequiz_scanned_essay_p");
$DB->execute("TRUNCATE TABLE offlinequiz_results");
$DB->execute("TRUNCATE TABLE offlinequiz_queue");
$DB->execute("TRUNCATE TABLE offlinequiz_queue_data");


echo 'Finished';