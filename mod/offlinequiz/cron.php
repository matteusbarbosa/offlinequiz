<?php
        // This file is part of mod_offlinequiz for Moodle - http://moodle.org/
        //
        // Moodle is free software: you can redistribute it and/or modify
        // it under the terms of the GNU General Public License as published by
        // the Free Software Foundation, either version 3 of the License, or
        // (at your option) any later version.
        //
        // Moodle is distributed in the hope that it will be useful,
        // but WITHOUT ANY WARRANTY; without even the implied warranty of
        // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        // GNU General Public License for more details.
        //
        // You should have received a copy of the GNU General Public License
        // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

        /**
        * The cron script called by a separate cron job to take load from the user frontend
        *
        * @package       mod
        * @subpackage    offlinequiz
        * @author        Juergen Zimmer <zimmerj7@univie.ac.at>
        * @copyright     2015 Academic Moodle Cooperation {@link http://www.academic-moodle-cooperation.org}
        * @since         Moodle 2.2+
        * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
        *
        **/

        use offlinequiz_result_import\offlinequiz_result_engine;
        use offlinequiz_result_import\offlinequiz_point;
        use offlinequiz_result_import\offlinequiz_result_page;

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        defined('MOODLE_INTERNAL') || die();

        define("OFFLINEQUIZ_MAX_CRON_JOBS", "15");
        define("OFFLINEQUIZ_TOP_QUEUE_JOBS", "5");

        require_once(dirname(__FILE__) . '/../../config.php');
        require_once($CFG->libdir . '/moodlelib.php');
        require_once($CFG->dirroot . '/mod/offlinequiz/evallib.php');
        require_once($CFG->dirroot . '/mod/offlinequiz/lib.php');
        require_once($CFG->dirroot . '/mod/offlinequiz/report/rimport/scanner.php');
        require_once($CFG->dirroot . '/mod/offlinequiz/report/rimport/scanner2.php');
        require_once($CFG->dirroot . '/mod/offlinequiz/report/rimport/positionslib.php');
        require_once($CFG->dirroot . '/mod/offlinequiz/report/rimport/essay_results_process.php');
        require_once($CFG->libdir . '/gradelib.php');



        function offlinequiz_evaluation_cron($jobid = 0, $verbose = false) {
        global $CFG, $DB;


        //raise_memory_limit(MEMORY_EXTRA);

        // Only count the jobs with status processing that have been started in the last 24 hours.
        $expiretime = (int) time() - 86400;
        $runningsql = "SELECT COUNT(*)
        FROM {offlinequiz_queue}
        WHERE status = 'processing'
        AND timestart > :expiretime
        AND timefinish = 0";
        $runningjobs = $DB->count_records_sql($runningsql, array('expiretime' => (int) $expiretime));

        if ($runningjobs >= OFFLINEQUIZ_MAX_CRON_JOBS) {
        echo "Too many jobs running! Exiting!";
        //return;
        }

 

        // TODO do this properly. Just for testing.
        $sql = "SELECT * FROM {offlinequiz_queue} WHERE status = 'new'";
        $params = array();
        if ($jobid) {
        
        $sql .= ' AND id = :jobid ';
        $params['jobid'] = $jobid;
        }
        $sql .= " ORDER BY id ASC";

        // If there are no new jobs, we simply exit.
        //can use 3th parameter as zero and OFFLINEQUIZ_TOP_QUEUE_JOBS as 4th parameter to limit top 5
        if (!$jobs = $DB->get_records_sql($sql, $params)) {


            if ($verbose) {
            echo get_string('nothingtodo', 'offlinequiz');
            }


            return;
        }


        $numberofjobs = count($jobs);

        if ($verbose) {
        $pbar = new progress_bar('offlinequizcronbar', 500, true);
        $pbar->create();
        $pbar->update(0, $numberofjobs,
        "Processing job - {0}/{$numberofjobs}.");
        }
        $numberdone = 0;

        foreach ($jobs as $job) {
        // Check whether the status is still 'new' (might have been changed by other cronjob).
        $transaction = $DB->start_delegated_transaction();
        $status = $DB->get_field('offlinequiz_queue', 'status', array('id' => $job->id));
        if ($status == 'new') {
        $DB->set_field('offlinequiz_queue', 'status', 'processing', array('id' => $job->id));
        $job->timestart = time();
        $DB->set_field('offlinequiz_queue', 'timestart', $job->timestart, array('id' => $job->id));
        } else {
            //already done
            continue;
        }
        $transaction->allow_commit();

        // Set up the context for this job.
        if (!$offlinequiz = $DB->get_record('offlinequiz', array('id' => $job->offlinequizid))) {
        $DB->set_field('offlinequiz_queue', 'status', 'error', array('id' => $job->id));
        $DB->set_field('offlinequiz_queue', 'info', 'offlinequiz not found', array('id' => $job->id));
        continue;
        }
        if (!$course = $DB->get_record('course', array('id' => $offlinequiz->course))) {
        $DB->set_field('offlinequiz_queue', 'status', 'error', array('id' => $job->id));
        $DB->set_field('offlinequiz_queue', 'info', 'course not found', array('id' => $job->id));
        continue;
        }

        if (!$cm = get_coursemodule_from_instance("offlinequiz", $offlinequiz->id, $course->id)) {
        $DB->set_field('offlinequiz_queue', 'status', 'error', array('id' => $job->id));
        $DB->set_field('offlinequiz_queue', 'info', 'course module found', array('id' => $job->id));
        continue;
        }
        if (!$context = context_module::instance($cm->id)) {
        $DB->set_field('offlinequiz_queue', 'status', 'error', array('id' => $job->id));
        $DB->set_field('offlinequiz_queue', 'info', 'context not found', array('id' => $job->id));
        continue;
        }
        if (!$groups = $DB->get_records('offlinequiz_groups', array('offlinequizid' => $offlinequiz->id),
        'groupnumber', '*', 0, $offlinequiz->numgroups)) {
        $DB->set_field('offlinequiz_queue', 'status', 'error', array('id' => $job->id));
        $DB->set_field('offlinequiz_queue', 'info', 'no offlinequiz groups found', array('id' => $job->id));
        continue;
        }
        $coursecontext = context_course::instance($course->id);

        offlinequiz_load_useridentification();

        $jobdata = $DB->get_records_sql("
        SELECT *
        FROM {offlinequiz_queue_data}
        WHERE queueid = :queueid
        AND status = 'new'",
        array('queueid' => $job->id));

        list($maxquestions, $maxanswers, $formtype, $questionsperpage) =
        offlinequiz_get_question_numbers($offlinequiz, $groups);

        $dirname = '';
        $doubleentry = 0;


        if(count($jobdata) == 0){
            echo 'linhas em offlinequiz_queue_data não encontradas';
            //chama próximo loop
            continue;
        } 

        if($job->type === 'essay'){

       // set_time_limit(120);

        $grades_formatted = [];

        foreach ($jobdata as $data) {
            $starttime = time();
            
            $DB->set_field('offlinequiz_queue_data', 'status', 'processing', array('id' => $data->id));                       
            
            $basename = pathinfo($data->filename, PATHINFO_BASENAME);
            $filename = explode('.pdf-', $basename);
            $userid = $filename[0];
            $qid = substr($filename[1], 0, 5);
            
            $grades_queue = $DB->get_records('offlinequiz_grade_essay', [
                'userid' => $job->studentid,
                'offlinequizid' => $offlinequiz->id,
                'timefinished' => 0
                ]);
                
                foreach($grades_queue as $k => $gd){
                    
                    if($job->grade_existent_action == 2){                        
                        $grade_reset = new \stdClass;
                        $grade_reset->userid = $job->studentid;
                        $grade_reset->rawgrade = null;
                        $grade_reset->finalgrade = null;
                        $grade_reset->overriden = 1;
                        offlinequiz_grade_item_update( $offlinequiz,  $grade_reset);
                    }
                    

                    //criar grades
                    if(!isset($grades_formatted[$job->studentid])){
                        $grades_formatted[$job->studentid] = new \stdClass;
                        $grades_formatted[$job->studentid]->rawgrade = 0;
                        $grades_formatted[$job->studentid]->userid = $job->studentid;
                        $grades_formatted[$job->studentid]->feedback = '';
                        
               
                    }
                    $grades_formatted[$job->studentid]->feedback .= $qid.") : ".$gd->feedback."\n";
                    
                    $grades_formatted[$job->studentid]->rawgrade += floatval($gd->rawgrade);
                    
                    $DB->set_field('offlinequiz_grade_essay', 'timefinished', time(), array('id' => $gd->id));
                }

                $DB->set_field('offlinequiz_queue_data', 'status', 'finished', array('id' => $data->id));
                
          
                }
                
            

  
            $updates = offlinequiz_grade_item_update( $offlinequiz,  $grades_formatted);
                
                //TODO: SUPORTE A GRUPOS
                $group = $DB->get_record('offlinequiz_groups', array('offlinequizid' => $job->offlinequizid,
                'groupnumber' => 1));
                
                
                $sql = "SELECT id
                FROM {offlinequiz_results}
                WHERE offlinequizid = :offlinequizid
                AND offlinegroupid = :offlinegroupid
                AND userid = :userid
                ORDER BY id ASC";
                
                
                $params = array('offlinequizid' => $offlinequiz->id, 'offlinegroupid' => $group->id, 'userid' => $job->studentid);
                
                $result = $DB->get_record_sql($sql, $params);
                
                
                if (!$result) {
                        // There is no result. First we have to clone the template question usage of the offline group.
                        // We have to use our own loading function in order to get the right class.
                        $templateusage = offlinequiz_load_questions_usage_by_activity($group->templateusageid);
                        
                        // Get the question instances for initial maxmarks.
                        $sql = "SELECT questionid, maxmark
                        FROM {offlinequiz_group_questions}
                        WHERE offlinequizid = :offlinequizid
                        AND offlinegroupid = :offlinegroupid";
                        
                        $qinstances = $DB->get_records_sql($sql,
                        array('offlinequizid' => $offlinequiz->id,
                        'offlinegroupid' => $group->id));
                        
                        // Clone it...
                        $quba = $templateusage->get_clone($qinstances);
                        
                        // And save it. The clone contains the same question in the same order and the same order of the answers.
                        \question_engine::save_questions_usage_by_activity($quba);
                        
                        $result = new \stdClass();
                        $result->offlinequizid = $offlinequiz->id;
                        $result->offlinegroupid = $group->id;
                        $result->userid = $job->studentid;
                        $result->teacherid = $job->importuserid;
                        $result->sumgrades = $grades_formatted[$job->studentid]->rawgrade;
                        $result->usageid = $quba->get_id();
                        $result->attendant = 'manualgrade';
                        $result->status = 'complete';
                        $result->timecreated = time();
                        $result->timemodified = time();
                        
                        $DB->insert_record('offlinequiz_results', $result);
                        
                    } else {
                        $DB->set_field('offlinequiz_results', 'sumgrades', $grades_formatted[$job->studentid]->rawgrade,
                        array('id' => $result->id));
                    }
                    
                    
                    $DB->set_field('offlinequiz_queue', 'status', 'finished', array('id' => $job->id));
                    
                    
                    
                    
                
                
            } else {
                //multi-choice
                
                foreach ($jobdata as $data) {
                    $starttime = time();
                    
                    $DB->set_field('offlinequiz_queue_data', 'status', 'processing', array('id' => $data->id));
                    
                    // We remember the directory name to be able to remove it later.
                    if (empty($dirname)) {
                        $pathparts = pathinfo($data->filename);
                        $dirname = $pathparts['dirname'];
                    }
                
                    
                    
                    try {
                        
                        $version = $offlinequiz->algorithmversion;
                        if ($version < 2) {
                            // Create a new scanner for every page.
                            $scanner = new offlinequiz_page_scanner($offlinequiz, $context->id, $maxquestions, $maxanswers);
                            // Try to load the image file.
                            echo 'job ' . $job->id . ': evaluating ' . $data->filename . "\n";
                            $scannedpage = $scanner->load_image($data->filename);
                            if ($scannedpage->status == 'ok') {
                                echo 'job ' . $job->id . ': image loaded ' . $scannedpage->filename . "\n";
                            } else if ($scannedpage->error == 'filenotfound') {
                                echo 'job ' . $job->id . ': image file not found: ' . $scannedpage->filename . "\n";
                            }
                            // Unset the origfilename because we don't need it in the DB.
                            unset($scannedpage->origfilename);
                            $scannedpage->offlinequizid = $offlinequiz->id;
                            
                            // If we could load the image file, the status is 'ok', so we can check the page for errors.
                            if ($scannedpage->status == 'ok') {
                                // We autorotate so check_scanned_page will return a potentially new scanner and the scannedpage.
                                list($scanner, $scannedpage) = offlinequiz_check_scanned_page($offlinequiz, $scanner, $scannedpage,
                                $job->importuserid, $coursecontext, true);
                            } else {
                                if (property_exists($scannedpage, 'id') && !empty($scannedpage->id)) {
                                    $DB->update_record('offlinequiz_scanned_pages', $scannedpage);
                                } else {
                                    $scannedpage->id = $DB->insert_record('offlinequiz_scanned_pages', $scannedpage);
                                }
                            }
                            echo 'job ' . $job->id . ': scannedpage id ' . $scannedpage->id . "\n";
                            
                            // If the status is still 'ok', we can process the answers. This potentially submits the page and
                            // checks whether the result for a student is complete.
                            if ($scannedpage->status == 'ok') {
                                // We can process the answers and submit them if possible.
                                $scannedpage = offlinequiz_process_scanned_page($offlinequiz, $scanner, $scannedpage,
                                $job->importuserid, $questionsperpage, $coursecontext, true);
                                echo 'job ' . $job->id . ': processed answers for ' . $scannedpage->id . "\n";
                            } else if ($scannedpage->status == 'error' && $scannedpage->error == 'resultexists') {
                                // Already process the answers but don't submit them.
                                $scannedpage = offlinequiz_process_scanned_page($offlinequiz, $scanner, $scannedpage,
                                $job->importuserid, $questionsperpage, $coursecontext, false);
                                
                                // Compare the old and the new result wrt. the choices.
                                $scannedpage = offlinequiz_check_different_result($scannedpage);
                            }
                            
                            // If there is something to correct then store the hotspots for retrieval in correct.php.
                            if ($scannedpage->status != 'ok' && $scannedpage->error != 'couldnotgrab'
                            && $scannedpage->error != 'notadjusted' && $scannedpage->error != 'grouperror') {
                                $scanner->store_hotspots($scannedpage->id);
                            }
                            
                            if ($scannedpage->status == 'ok' || $scannedpage->status == 'submitted'
                            || $scannedpage->status == 'suspended' || $scannedpage->error == 'missingpages') {
                                // Mark the file as processed.
                                $DB->set_field('offlinequiz_queue_data', 'status', 'processed', array('id' => $data->id));
                            } else {
                                $DB->set_field('offlinequiz_queue_data', 'status', 'error', array('id' => $data->id));
                                $DB->set_field('offlinequiz_queue_data', 'error', $scannedpage->error, array('id' => $data->id));
                            }
                            if ($scannedpage->error == 'doublepage') {
                                $doubleentry++;
                            }
                        } else {
                            $contextid = 0;
                            $engine = new offlinequiz_result_engine($offlinequiz, $context->id, $data->filename, 0);
                            $resultpage = $engine->scanpage();
                            $engine->save_page(2);
                        }
                        
                        offlinequiz_update_grades($offlinequiz);
                        
                    } catch (Exception $e) {     
                        
                        
                        if(!isset($scannedpage)){
                            $scannedpage = new \stdClass();
                        }
                        echo 'job ' . $job->id . ': ' . $e->getMessage() . "\n";
                        $DB->set_field('offlinequiz_queue_data', 'status', 'error', array('id' => $data->id));
                        $DB->set_field('offlinequiz_queue_data', 'error', 'couldnotgrab', array('id' => $data->id));
                        $DB->set_field('offlinequiz_queue_data', 'info', $e->getMessage(), array('id' => $data->id));
                        $scannedpage->status = 'error';
                        $scannedpage->error = 'couldnotgrab';
                        if ($scannedpage->id) {
                            $DB->update_record('offlinequiz_scanned_pages', $scannedpage);
                        } else {
                            $DB->insert_record('offlinequiz_scanned_pages', $scannedpage);
                        }
                        
                        
                    }
                    
                }
                
                
                
                
                
            } // End foreach jobdata.
            
            
            
            
            
            $job->timefinish = time();
            $DB->set_field('offlinequiz_queue', 'timefinish', $job->timefinish, array('id' => $job->id));
            $job->status = 'finished';
            $DB->set_field('offlinequiz_queue', 'status', 'finished', array('id' => $job->id));
            
            echo date('Y-m-d-H:i') . ": Import queue with id $job->id imported.\n\n";
            
            if ($user = $DB->get_record('user',  array('id' => $job->importuserid))) {
                $mailtext = get_string('importisfinished', 'offlinequiz', format_text($offlinequiz->name, FORMAT_PLAIN));
                
                // How many pages have been imported successfully.
                $countsql = "SELECT COUNT(id)
                FROM {offlinequiz_queue_data}
                WHERE queueid = :queueid
                AND status = 'processed'";
                $params = array('queueid' => $job->id);
                
                $mailtext .= "\n\n". get_string('importnumberpages', 'offlinequiz', $DB->count_records_sql($countsql, $params));
                
                // How many pages have an error.
                $countsql = "SELECT COUNT(id)
                FROM {offlinequiz_queue_data}
                WHERE queueid = :queueid
                AND status = 'error'";
                
                $mailtext .= "\n". get_string('importnumberverify', 'offlinequiz', $DB->count_records_sql($countsql, $params));
                
                $mailtext .= "\n". get_string('importnumberexisting', 'offlinequiz', $doubleentry);
                
                $linkoverview = "$CFG->wwwroot/mod/offlinequiz/report.php?q={$job->offlinequizid}&mode=overview";
                $mailtext .= "\n\n". get_string('importlinkresults', 'offlinequiz', $linkoverview);
                
                $linkupload = "$CFG->wwwroot/mod/offlinequiz/report.php?q={$job->offlinequizid}&mode=rimport";
                $mailtext .= "\n". get_string('importlinkverify', 'offlinequiz', $linkupload);
                
                $mailtext .= "\n\n". get_string('importtimestart', 'offlinequiz', userdate($job->timestart));
                $mailtext .= "\n". get_string('importtimefinish', 'offlinequiz', userdate($job->timefinish));
                
                email_to_user($user, $CFG->noreplyaddress, get_string('importmailsubject', 'offlinequiz'), $mailtext);
            }
      
        $numberdone++;
        if ($verbose) {
            ob_flush();
            $pbar->update($numberdone, $numberofjobs,
            "Processing job - {$numberdone}/{$numberofjobs}.");
        }

        } // End foreach.

        } // End function.

        require_once($CFG->libdir . '/clilib.php');
        list($options, $unrecognized) = cli_get_params(array('cli' => false), array('h' => 'help'));

        if (array_key_exists('cli', $options) && $options['cli']) {
        echo date('Y-m-d-H:i') . ': ';
        offlinequiz_evaluation_cron(null, true);
        echo " done.\n";
        die();
        }