<?php
// This file is part of mod_offlinequiz for Moodle - http://moodle.org/
    //
    // Moodle is free software: you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation, either version 3 of the License, or
    // (at your option) any later version.
    //
    // Moodle is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
    /**
    * form for uploading scanned documents in the rimport report
    *
    * @package       mod
    * @subpackage    offlinequiz
    * @author        Juergen Zimmer <zimmerj7@univie.ac.at>
    * @copyright     2015 Academic Moodle Cooperation {@link http://www.academic-moodle-cooperation.org}
    * @since         Moodle 2.1+
    * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
    *
    **/
    defined('MOODLE_INTERNAL') || die();
    require_once($CFG->libdir.'/formslib.php');
    class essay_grade_form extends moodleform {
        private $files;
        private $files_grades;
        private $userid;
        private $offlinequiz;
        private $contextid;
        private $dirname;
        public function __construct($offlinequiz, $contextid, $dirname) {
            $this->offlinequiz = $offlinequiz;
            $this->contextid = $contextid;
            $this->userid = null;
            $this->dirname = $dirname;
            parent::__construct();
        }
        public function loadEssayQuestions(){
            global $DB;
            $groups = $DB->get_records('offlinequiz_groups', array('offlinequizid' => $this->offlinequiz->id), 'groupnumber',
            '*', 0, $this->offlinequiz->numgroups);
            $letterstr = 'ABCDEFGHIJKL';
            // Process group data.
            foreach ($groups as $group) {
                $groupletter = $letterstr[$group->groupnumber - 1];
                // Load all the questions needed for this offline quiz group.
                $sql = "SELECT q.*, c.contextid, ogq.page, ogq.slot, ogq.maxmark
                FROM {offlinequiz_group_questions} ogq,
                {question} q,
                {question_categories} c
                WHERE ogq.offlinequizid = :offlinequizid
                AND ogq.offlinegroupid = :offlinegroupid
                AND q.id = ogq.questionid
                AND q.category = c.id
                AND q.qtype = 'essay'
                ORDER BY ogq.slot ASC ";
                $params = array('offlinequizid' => $this->offlinequiz->id, 'offlinegroupid' => $group->id);
                $questions = $DB->get_records_sql($sql, $params);
                return $questions;
            }
        }
        public function getStudentID($user_id_field, $student_key){
            global $DB;
            //especial para uninga substituindo hífen
            $student_key = preg_replace('/\D/', '', $student_key);
            return $DB->get_field_sql("SELECT id FROM {user} WHERE REPLACE($user_id_field, '-', '') = '$student_key'");
        }
        public function getFiles($user_id_field){
            global $DB;
            $out = array();
            $fs = get_file_storage();
            $files = get_directory_list($this->dirname);
            foreach ($files as $k => $file) {
                $filename = explode('-', pathinfo($file, PATHINFO_FILENAME));
                $student_key = substr($filename[0], 0, -4);
                $student_id = $this->getStudentID($user_id_field, $student_key);
                $out[$student_id][] = $this->dirname . '/' . $file;
            }
            return $out;
        }
        public function mapFilesToGrades($user_id_field){
            global $DB;
            $grade_inputs = [];
            //obter dissertativas do quiz em array
            $questions = $this->loadEssayQuestions();
            $files = get_directory_list($this->dirname);
            $data = [];
            foreach($files as $k => $file_item){
                $filename_basic = pathinfo($file_item, PATHINFO_FILENAME);
                $upload_info = explode('.pdf-', $filename_basic);
                if(!isset($data[$upload_info[0]])){
                    $data[$upload_info[0]] = [];
                }
                //pula folha de identificação do aluno
                if((int) $upload_info[1] > 0){
                    $data[$upload_info[0]][$upload_info[1]] = new \stdClass();
                }
            }
            foreach($data as $student_key => $data_items){
                $student_id = $this->getStudentID($user_id_field, $student_key);
                $questions_copy = $questions;
                foreach($data_items as $k => $data_item){
                    $filename_basic = pathinfo($file_item, PATHINFO_FILENAME);
                    $upload_info = explode('.pdf-', $filename_basic);
                    if(empty($questions_copy)){
                        throw new \Exception('Esta avaliação contém '.count($questions).' questões. A imagem  '.($k+1).' do aluno '.$student_id.' não foi associada a nenhuma questão. Verifique número de questões contra o número de imagens enviadas.');
                        break;
                    }
                    $question_target = array_shift($questions_copy);
                    $grade_input = new \stdClass();
                    //get first char of first index
                    $grade_input->student_key = $student_key;
                    $grade_input->student_id = $student_id;
                    //+1 because start from 0
                    $grade_input->question_id = $question_target->id;
                    $grade_input->grade_raw = floatval($questions[$grade_input->question_id]->maxmark);
                    $grade_input->maxmark = floatval($questions[$grade_input->question_id]->maxmark);
                    $grade_input->feedback = floatval($questions[$grade_input->question_id]->maxmark);
                    $grade_inputs[$student_id][$grade_input->question_id] = $grade_input;
                }
            }
            return $grade_inputs;
        }
        public function definition() {
            global $DB, $OUTPUT;
            $mform =& $this->_form;
            if (! $course = $DB->get_record("course", array('id' => $this->offlinequiz->course))) {
                print_error("The course with id $this->offlinequiz->course that the offlinequiz with id $this->offlinequiz->id belongs to is missing");
            }
            if (! $cm = get_coursemodule_from_instance("offlinequiz", $this->offlinequiz->id, $course->id)) {
                print_error("The course module for the offlinequiz with id $this->offlinequiz->id is missing");
            }
            // -------------------------------------------------------------------------------
            // The file to import.
            $mform->addElement('header', 'importfileupload', 'Atribua nota para cada dissertativa');
            $qtype = optional_param('qtype', '', PARAM_ALPHA);
            $mform->addElement('hidden', 'qtype', 'essay');
            $mform->setType('qtype', PARAM_ALPHA);  
            $mform->addElement('hidden', 'q', $this->offlinequiz->id);
            $mform->setType('q', PARAM_INT);  
            $mform->addElement('hidden', 'action', 'grade_essay');
            $mform->setType('action', PARAM_RAW);  
            $mform->addElement('hidden', 'mode', 'rimport');
            $mform->setType('mode', PARAM_ALPHA);  
            $mform->addElement('hidden', 'essay_stage_confirm', 1);
            $mform->setType('essay_stage_confirm', PARAM_INT);
            //associar cada dissertativa com um arquivo ok
            //emitir exceção e encerrar caso não seja compatível
            $grade_inputs = [];
            //start accordion
            $offlinequiz_useridentification = offlinequiz_get_config('useridentification');
            $user_id_field = (explode('=', $offlinequiz_useridentification))[1];
            $grade_inputs = $this->mapFilesToGrades($user_id_field);
            $files = $this->getFiles($user_id_field);
            $mform->addElement('html','<table border="1" width="100%"  cellpadding="10" style="overflow-x: scroll;">');
            $mform->addElement('html', '<tr bgcolor="black" style="color: white;">
            <th width="100">Aluno(a)</th>
            <th width="50">Questão</th>
            <th width="100">Valor da questão</th>
            <th width="120">Prévia</th>
            <th width="50">Nota do aluno</th>
            <th>Feedback</th>
            </tr>');
            $notifications = [];

            $grade_existent_action = optional_param('grade_existent_action', 1, PARAM_INT);
            $mform->addElement('hidden', 'grade_existent_action',  $grade_existent_action);
            $mform->setType('grade_existent_action', PARAM_RAW);

            if($grade_existent_action == 1){
                $grades = grade_get_grades($course->id, 'mod', 'offlinequiz',  $this->offlinequiz->id, array_keys($grade_inputs));
                foreach($grades->items[0]->grades as $s_id => $s_grade){
                    if(isset($grade_inputs[$s_id]) && !empty($s_grade->grade)){
                        //if filled grade: ignore, remove from evaluation list
                        unset($grade_inputs[$s_id]);
                    }
                }
            }

            //no 
            if(empty($grade_inputs)){
                $mform->addElement('html', $OUTPUT->notification(get_string('empty_ignored_submit', 'mod_offlinequiz'), 'notifywarning'));
                return;
            }

            foreach($grade_inputs as $student_id => $questions){
                //start accordion section content
                $user = $DB->get_record("user", ['id' => $student_id], 'id,firstname,lastname,email');
        
                //Pula a folha de info do aluno
                $q_walker = 1;
                foreach($questions as $q_id => $grade_input){
                    $mform->addElement('html', '<tr>');
           
                    if($q_walker  == 1){
                        $mform->addElement('html', '<th width="50" rowspan="'.count($questions).'">['.$user_id_field.':'.$student_id.'] '.$user->firstname.' '.$user->lastname.' <small style="float:right">'.$user->email.'</small></th>');
                    }
                    $grade_input_name = 'grade_input['.$student_id.']['.$grade_input->question_id.']';
                    $feedback_input_name = 'feedback_input['.$student_id.']['.$grade_input->question_id.']';
                    $file_name = 'file_input['.$student_id.']['.$q_id.']';
                    $mform->addElement('hidden', $file_name, $files[$student_id][$q_walker]);
                    $mform->setType($file_name, PARAM_RAW);   
                    $mform->addElement('html', '<td>'.$q_walker.'</td>');
                    $mform->addElement('html', '<td style="text-align:center;">');
                    $mform->addElement('html', offlinequiz_format_grade($this->offlinequiz, $grade_input->maxmark));
                    $mform->addElement('hidden', 'grade_expected', '', 'readonly="readonly" maxlength="10"');
                    $mform->setType('grade_expected', PARAM_RAW);   
                    $mform->setDefault('grade_expected', offlinequiz_format_grade($this->offlinequiz, $grade_input->maxmark));   
                    $mform->addElement('html', '</td>');
                    $mform->addElement('html', '<td  style="text-align:center;">');
                    $mform->addElement('html', '<a href="./report/rimport/file_preview.php?target='.urlencode($files[$student_id][$q_walker]).'" target="_blank">
                    <img src="./pix/icon-download.png">
                    </a>');
                    $mform->addElement('html', '</td>');
                    $mform->addElement('html', '<td>');
                    $mform->addElement('html', '<input type="number" name="'.$grade_input_name.'" style="width: 130px;" min="0" step="any" pattern="^\d*(\.\d{0,2})?$" max="'.offlinequiz_format_grade($this->offlinequiz, $grade_input->maxmark).'">');
                    $grade_input->grade_raw = floatval($grade_input->maxmark);
                    $mform->setDefault($grade_input_name, $grade_input->grade_raw);   
                    $mform->setType($grade_input_name, PARAM_RAW);   
                    $mform->addElement('html', '</td>');
                    $mform->addElement('html', '</td>');
                    $mform->addElement('html', '<td><input name="'.$feedback_input_name.'" style="width:100%" wrap="virtual" width="100%"></td>');
                    $mform->setType($feedback_input_name, PARAM_ALPHANUMEXT);   
                    $q_walker++;
                    //end accordion section content
                    $mform->addElement('html', '</tr>');
                }
            }
            if(count($notifications) > 0){
                // Notify the user.
                foreach($notifications as $n){
                    $mform->addElement('html', $OUTPUT->notification($n, 'notifywarning'));
                }
            }
            //end accordion
            $mform->addElement('html', '</table>');
            // Submit button.
            $mform->addElement('html', html_writer::link(new moodle_url('/mod/offlinequiz/report.php', array('qtype'=>'essay', 'mode' => 'rimport', 'q'=> $this->offlinequiz->id)), 'Voltar', ['class' => 'btn btn-secondary', 'style' => "margin-top: 24px; width: 200px;float: right;"]));
            $mform->addElement('submit', 'submitbutton', 'Importar notas', 'style="margin-top: 24px; width: 200px;float: right;"');
        }
    }