<?php
// This file is part of mod_offlinequiz for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form for uploading scanned documents in the rimport report
 *
 * @package       mod
 * @subpackage    offlinequiz
 * @author        Juergen Zimmer <zimmerj7@univie.ac.at>
 * @copyright     2015 Academic Moodle Cooperation {@link http://www.academic-moodle-cooperation.org}
 * @since         Moodle 2.1+
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');


class offlinequiz_upload_form extends moodleform {

    public function definition() {
        $mform =& $this->_form;
        // -------------------------------------------------------------------------------
        // The file to import.
        $mform->addElement('header', 'importfileupload', get_string('importforms', 'offlinequiz_rimport'));
        $qtype = optional_param('qtype', '', PARAM_ALPHA);

        if(!$qtype){
            $mform->addElement('filepicker', 'newfile', 'Gabaritos de múltipla-escolha', null,
            array('subdirs' => 0, 'accepted_types' =>
                    array('.jpeg', 'JPEG', 'JPG', 'jpg', '.png', '.zip',
                          '.ZIP', '.tif', '.TIF', '.tiff', '.TIFF' , ".pdf", ".PDF")));

    $mform->addRule('newfile', null, 'required', null, 'client');

        } else {
            $mform->addElement('hidden', 'qtype', 'essay');
            $mform->setType('qtype', PARAM_RAW);

            $mform->addElement('hidden', 'action', 'grade_essay');
            $mform->setType('action', PARAM_ALPHANUMEXT);  

            $mform->addElement('hidden', 'qtype', 'essay');
            $mform->setType('qtype', PARAM_ALPHA);  


            $mform->addElement('hidden', 'mode', 'rimport');
            $mform->setType('mode', PARAM_ALPHA);  


    
            $mform->addElement('filepicker', 'essayfiles', 'ZIP de dissertativas <br>
            <a href="./report/rimport/file_preview.php?target='.urlencode(dirname(__FILE__).'/batch-essay-import-example.zip').'" target="_blank">
                <img src="./pix/icon-download.png" style="width:15px;height: 15px;"> Confira um modelo
                </a>
            
           ', '',
            array('subdirs' => 0, 'accepted_types' =>
                    array('.zip')));

                    
        
                $mform->addRule('essayfiles', null, 'required', null, 'client');

                $radioarray=array();
                $radioarray[] = $mform->createElement('radio', 'grade_existent_action', '', 'Ignorar', 1, $attributes=[]);
                $radioarray[] = $mform->createElement('radio', 'grade_existent_action', '', 'Redefinir', 2, $attributes=[]);
                $mform->addGroup($radioarray, 'radioar', 'Ação para notas existentes:', array(' '), false);
                $mform->setDefault('grade_existent_action', 1);
     

        }

        // Submit button.
        $mform->addElement('submit', 'submitbutton', get_string('import', 'offlinequiz_rimport'));
    }
}