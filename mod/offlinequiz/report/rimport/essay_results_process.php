<?php

/* 
Primeiro ocorre a criação dos results
Primeiro ocorre criação do grade_grades
depois ocorre o registro do sumgrades
*/
defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir.'/formslib.php');

function convert_black_white($file, $threshold) {
    global $CFG;
    $cv = offlinequiz_get_config('convertcommand');
    $command = $cv." " . realpath($file) . " -threshold $threshold% " . realpath($file);
    popen($command, 'r');
}
//criar job com userid por usuário
//no cron, identificar esse agrupamento por userid e atribuir sumgrade 
function process_essay_post_confirm(){
    global $DB, $USER, $OUTPUT;
    
    $offlinequizid = required_param('q', PARAM_INT); // Result ID.
    if(empty($_POST['grade_input'])){
        debugging('grade_input must be provided ');
    }
    
    $grades = [];
    foreach($_POST['grade_input'] as $student_id => $questions){
        
        // Create a new queue job.
        $job = new stdClass();
        $job->offlinequizid = $offlinequizid;
        $job->importuserid = $USER->id;
        $job->studentid = $student_id;
        $job->type = 'essay';
        $job->timecreated = time();
        $job->timestart = 0;
        $job->timefinish = 0;
        $job->grade_existent_action = $_POST['grade_existent_action'];
        $job->status = 'new'; // or uploading?
        if (!$job->id = $DB->insert_record('offlinequiz_queue', $job)) {
            //         echo $OUTPUT->notification(get_string('couldnotcreatejob', 'offlinequiz_rimport'), 'notifyproblem');
        }
        
        $threshold = get_config('offlinequiz', 'blackwhitethreshold');
        // Add the files to the job.
        
        
        foreach($questions as $qid => $rawgrade){
            
            $pregrade_existent = $DB->get_records('offlinequiz_grade_essay', [
                'userid' => $job->studentid,
                'questionid' => $qid,
                'offlinequizid' => $offlinequizid
                ]);
                
                //TODO : Atualizar nota ou ignorar?
                /* if(!empty($pregrade_existent->timefinished)){
                    echo $OUTPUT->notification('Já existe nota para aluno(a) '.$student_id.' na questão '.$qid, 'notifysuccess');
                    continue;
                } */
                
                
                if(!empty($pregrade_existent) && $job->grade_existent_action == 1){
                    continue;
                }
        
                    $grade = new \stdClass;
                    $grade->offlinequizid = $offlinequizid;
                    $grade->questionid = $qid;
                    $grade->feedback = $_POST['feedback_input'][$student_id][$qid];
                    $grade->userid = $student_id;
                    $grade->rawgrade = floatval($rawgrade);
                    $grade->queueid = $job->id;
                    $grade->timecreated = time();
                    $grade->timeupdated = time();
                    $grades[] = $grade;    
            
                
                $filepath = $_POST['file_input'][$student_id][$qid];
                
                if ($threshold && $threshold > 0 && $threshold < 100) {
                    convert_black_white($filepath, $threshold);
                }
                $jobfile = new \stdClass();
                $jobfile->queueid = $job->id;
                $jobfile->filename = $filepath;
                
                $jobfile->status = 'new';
                $DB->insert_record('offlinequiz_queue_data', $jobfile);

                $scanned_essay_page = new \stdClass();
                $scanned_essay_page->questionid = $qid;
                $scanned_essay_page->userid = $student_id;
                $scanned_essay_page->answer_link = $filepath;
                $scanned_essay_page->offlinequizid = $offlinequizid;
                $scanned_essay_page->timecreated = time();

                $DB->insert_record('offlinequiz_scanned_essay_p', $scanned_essay_page);
                
            }
            
            
        }
        
        
        
        $DB->insert_records('offlinequiz_grade_essay', $grades);
        

        echo $OUTPUT->notification('Respostas de '.count($_POST['grade_input']).' alunos foram enviadas para a fila.', 'notifysuccess');
        echo html_writer::link(new moodle_url('/mod/offlinequiz/report.php', array('mode' => 'rimport', 'q'=> $offlinequizid)), 'Importar multipla escolha', ['class' => 'btn btn-primary']);
        echo ' - ';
        echo html_writer::link(new moodle_url('/mod/offlinequiz/report.php', array('qtype'=>'essay', 'mode' => 'rimport', 'q'=> $offlinequizid)), 'Importar dissertativas', ['class' => 'btn btn-primary']);
    }
    /**
    * @param dirname
    * @param importfile
    */
    function extract_pdf_to_tiff($dirname, $importfile) {
        global $CFG;
        // Extract each page to a separate file.
        $newfile = "$importfile-%d.tiff";
        $cv = offlinequiz_get_config('convertcommand');
        $handle = popen($cv.' -type grayscale -density 300 '.$importfile.' '.$newfile, 'r');
        fread($handle, 1);
        while (!feof($handle)) {
            fread($handle, 1);
        }
        
        pclose($handle);
        
        if (count(get_directory_list($dirname)) > 1) {
            // It worked, remove original.
            unlink($importfile);
        } else {
            throw new \Exception('Não foi possível gerar os TIFFS. Verifique instalação de Imagick e Ghost script para rodar comando convert apropriadamente');
        }
        $files = get_directory_list($dirname);
        return $files;
    }
    
    function process_essay_upload_stage($offlinequiz, $upload_form, $contextid){
        global $CFG, $COURSE, $DB, $OUTPUT, $USER;
        // Work out if this is an uploaded file
        // or one from the filesarea.
        $unique = str_replace('.', '', microtime(true) . rand(0, 100000));
        $dirname = "{$CFG->tempdir}/offlinequiz/import/$unique";

 
        
        if($_POST['grade_existent_action'] == 2){
            echo $OUTPUT->notification('Você escolheu "Redefinir" as notas. Ao clicar em "Importar notas", as notas dos alunos em questão serão zeradas e redefinidas.', 'notifysuccess');
        } else {
            echo $OUTPUT->notification('Você escolheu "Ignorar" as notas. Ao clicar em "Importar notas", apenas as notas dos alunos que ainda não foram avaliadas serão consideradas.', 'notifysuccess');
        }



        require_once($CFG->dirroot . '/mod/offlinequiz/report/rimport/upload_form.php');
        
        $realfilename = $upload_form->get_new_filename('essayfiles');
        // Create a unique temp dir.
        check_dir_exists($dirname, true, true);
        $importfile = $dirname . '/' . $realfilename;
        if (!$result = $upload_form->save_file('essayfiles', $importfile, true)) {
            throw new moodle_exception('uploadproblem');
        }
        $files = array();
        $mimetype = mimeinfo('type', $importfile);
        if ($mimetype == 'application/zip') {
            $fp = get_file_packer('application/zip');
            $files = $fp->extract_to_pathname($importfile, $dirname);
            $user_ids = [];
            if ($files) {
                unlink($importfile);
                $files = get_directory_list($dirname);
                foreach ($files as $file) {
                    $mimetype = mimeinfo('type', $file);
                    if ($mimetype == 'application/pdf') {
                        $user_ids[] = $file;
                        extract_pdf_to_tiff($dirname, $dirname . '/' . $file);
                    }
                }
                $files = get_directory_list($dirname);
            } else {
                echo $OUTPUT->notification(get_string('couldnotunzip', 'offlinequiz_rimport', $realfilename), 'notifyproblem');
            }
        } else if ($mimetype == 'application/pdf') {
            $files = extract_pdf_to_tiff ( $dirname, $importfile );
        } else if (preg_match('/^image/' , $mimetype)) {
            $files[] = $realfilename;
        }
        $added = count($files);
        $user_count = count($user_ids);

                      // Notify the user.
                      echo $OUTPUT->notification('Identificadas '.($added-1).' Respostas de '.$user_count.' alunos no ZIP enviado.', 'notifysuccess');

        
        require_once($CFG->dirroot . '/mod/offlinequiz/report/rimport/essay_grade_form.php');
        $essay_grade_form = new essay_grade_form($offlinequiz, $contextid, $dirname);
        $essay_grade_form->display();
        // die;
        

 
  
    }
