<?PHP
// This file is part of mod_offlinequiz for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The English language strings for offlinequizzes
 *
 * @package       mod
 * @subpackage    offlinequiz
 * @author        Juergen Zimmer <zimmerj7@univie.ac.at>
 * @copyright     2015 Academic Moodle Cooperation {@link http://www.academic-moodle-cooperation.org}
 * @since         Moodle 2.2+
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 **/

$string['modulename'] = 'Teste offline';
$string['modulenameplural'] = 'Questionários offline';
$string['pluginname'] = 'Teste offline';
$string['id'] = 'ID';
$string['ziporimagefileopenq'] = 'Subir abertas questões';
$string['addnewquestion'] = 'uma nova questão';
$string['add'] = 'Adicionar';
$string['addlist'] = 'Adicionar lista';
$string['addarandomselectedquestion'] = 'Adicionar uma questão selecionada aleatoriamente ...';
$string['addnewpagesafterselected'] = 'Adicionar quebras de página após as perguntas selecionadas';
$string['addnewquestionsqbank'] = 'Adicionar questões à categoria {$a->catname}: {$a->link}';
$string['addnewuseroverride'] = 'Adicionar substituição de usuário';
$string['addpagebreak'] = 'Adicionar quebra de página';
$string['addpagehere'] = 'Adicionar página aqui';
$string['addparts'] = 'Adicionar participantes';
$string['addquestionfrombankatend'] = 'Adicionar do banco de questões no final';
$string['addquestionfrombanktopage'] = 'Adicionar do banco de questões à página {$a}';
$string['addrandom'] = 'Adicionar aleatoriamente {$a} questão (s) de múltipla escolha';
$string['addrandomfromcategory'] = 'perguntas aleatoriamente';
$string['addrandomquestion'] = 'perguntas aleatoriamente';
$string['addrandomquestiontoofflinequiz'] = 'Adicionando perguntas ao offlinequiz {$a->name} (group {$a->group})';
$string['addrandomquestiontopage'] = 'Adicionar uma pergunta aleatória à página {$a}';
$string['addarandomquestion'] = 'perguntas aleatoriamente';
$string['addarandomquestion_help'] = 'O Moodle adiciona uma seleção aleatória de questões de múltipla escolha (ou questões de múltipla escolha tudo ou nada) ao grupo de questionários offline atual. O número de perguntas adicionadas pode ser definido. As perguntas são escolhidas a partir da categoria de pergunta atual (e, se selecionado, suas subcategorias). ';
$string['addtoofflinequiz'] = 'Adicionar ao questionário offline';
$string['addtoqueue'] = 'Adicionar à fila';
$string['allinone'] = 'Ilimitado';
$string['alllists'] = 'Todas as listas';
$string['allornothing'] = 'Tudo ou nada';
$string['allresults'] = 'Mostrar todos os resultados';
$string['allstudents'] = 'Mostrar todos os alunos';
$string['alwaysavailable'] = 'Sempre disponível';
$string['analysis'] = 'Análise do item';
$string['answerformforgroup'] = 'Formulário de resposta para o grupo {$a}';
$string['answerform'] = 'Formulário de respostas';
$string['answerforms'] = 'Formulários de respostas';
$string['answerpdfxy'] = 'Formulário para respostas ({$a} ->maxquestions questions / {$a} ->maxanswers options)';
$string['areyousureremoveselected'] = 'Tem certeza que deseja remover todas as questões selecionadas?';
$string['attemptexists'] = 'Existe tentativa';
$string['attemptsexist'] = 'Você não pode mais adicionar ou remover perguntas.';
$string['attemptsnum'] = 'Resultados: {$a}';
$string['attemptsonly'] = 'Mostrar alunos apenas com resultados';
$string['attendances'] = 'Presenças';
$string['blackwhitethreshold'] = 'Limiar preto / branco.';
$string['basicideasofofflinequiz'] = 'As ideias básicas para fazer questionários offline';
$string['bulksavegrades'] = 'Salvar notas';
$string['calibratescanner'] = 'Calibrar scanner';
$string['cannoteditafterattempts'] = 'Você não pode adicionar ou remover perguntas porque já existem resultados completos. ({$a}) ';
$string['category'] = 'Categoria';
$string['changed'] = 'Resultado foi alterado.';
$string['changeevaluationmode'] = 'Alterar a forma como a avaliação é feita. Só é possível se permitido pelo administrador. ';
$string['checkparts'] = 'Marcar participantes selecionados como presentes';
$string['checkuserid'] = 'Verificar grupo / ID do usuário';
$string['chooseagroup'] = 'Escolha um grupo ...';
$string['closebeforeopen'] = 'Não foi possível atualizar o questionário offline. Você especificou uma data de fechamento antes da data de abertura. ';
$string['closestudentview'] = 'Fechar Visualização do Aluno';
$string['closewindow'] = 'Fechar janela';
$string['cmmissing'] = 'O módulo do curso para o questionário offline com ID {$a} está faltando';
$string['configblackwhitethreshold'] = 'Define o limite para a conversão de preto / branco entre 1-99 - Quanto mais alto, mais branco deve ser o pixel importado para ser reconhecido como branco. Qualquer outro valor não leva a nenhuma conversão preto / branco. ';
$string['configdisableimgnewlines'] = 'Esta opção desativa novas linhas antes e depois das imagens nas folhas de perguntas em PDF. Aviso: Isso pode causar problemas de formatação. ';
$string['configexperimentalevaluation'] = 'Avaliação experimental das folhas de resposta';
$string['configexperimentalevaluationdesc'] = 'NÃO USE NA PRODUÇÃO! Esta opção é usada para testes alfa do novo algoritmo de avaliação. Se esta opção estiver configurada para sim, você pode ativá-la nas opções da instância offlinequiz. É altamente desencorajado o uso dessa opção. Não há suporte para dados perdidos devido à ativação desta opção. ';
$string['configintro'] = 'Os valores que você definir aqui são usados ​​como valores padrão para as configurações de novos questionários off-line.';
$string['configkeepfilesfordays'] = 'Determine por quantos dias os arquivos de imagem enviados são mantidos em armazenamento temporário. Durante esse tempo, os arquivos de imagem ficam disponíveis no relatório de administração do questionário offline. ';
$string['configonlylocalcategories'] = 'Não são permitidas categorias de perguntas compartilhadas.';
$string['configshuffleanswers'] = 'Respostas aleatórias';
$string['configshufflequestions'] = 'Se você habilitar esta opção, a ordem das perguntas nos grupos de questionários offline será aleatoriamente embaralhada cada vez que você recriar a visualização na guia "Criar formulários".';
$string['configshufflewithin'] = 'Se você habilitar esta opção, as respostas das questões de múltipla escolha são embaralhadas separadamente para cada grupo de questionários offline.';
$string['configuseridentification'] = 'Uma fórmula que descreve a identificação do usuário. Esta fórmula é usada para atribuir formulários de resposta a usuários no sistema. O lado direito da equação deve denotar um campo na tabela do usuário do Moodle. ';
$string['configpapergray'] = 'valor branco do papel, que é usado para a avaliação das folhas de resposta';
$string['configshufflewithin'] = 'Se activar esta opção, as partes que constituem as questões individuais serão misturadas aleatoriamente quando os formulários de perguntas e respostas forem criados.';
$string['confirmremovequestion'] = 'Tem certeza que deseja remover esta {$a} questão?';
$string['copyright'] = '<strong> Aviso: os textos desta página são apenas para sua informação pessoal. Como qualquer outro texto, essas questões estão sujeitas a restrições de direitos autorais. Você não tem permissão para copiá-los ou mostrá-los a outras pessoas! </strong> ';
$string['copy'] = 'Copiar';
$string['correct'] = 'correto';
$string['correcterror'] = 'resolver';
$string['correctforgroup'] = 'Respostas corretas para o Grupo {$a}';
$string['correctionform'] = 'Correção';
$string['correctionforms'] = 'Formulários de correção';
$string['correctionoptionsheading'] = 'Opções de correção';
$string['correctupdated'] = 'Formulário de correção atualizado para o grupo {$a}.';
$string['couldnotgrab'] = 'Não foi possível capturar a imagem {$a}';
$string['couldnotregister'] = 'Não foi possível registrar o usuário {$a}';
$string['createcategoryandaddrandomquestion'] = 'Criar categoria e adicionar pergunta aleatória';
$string['createofflinequiz'] = 'Criar formulários';
$string['createlistfirst'] = 'Adicionar participantes';
$string['createpartpdferror'] = 'O formulário PDF para a lista de participantes {$a} não pôde ser criado. A lista pode estar vazia. ';
$string['createpdferror'] = 'O formulário para o grupo {$a} não pôde ser criado. Talvez não haja perguntas no grupo. ';
$string['createpdffirst'] = 'Criar lista de PDF primeiro';
$string['createpdfforms'] = 'Criar formulários';
$string['createpdf'] = 'Formulário';
$string['createpdfs'] = 'Baixar formulários';
$string['createpdfsparticipants'] = 'Formulários PDF para lista de participantes';
$string['createquestionandadd'] = 'Crie uma nova pergunta e adicione-a ao questionário.';
$string['createquiz'] = 'Criar formulários';
$string['csvfile'] = 'Arquivo CSV';
$string['csvformat'] = 'Arquivo de texto com valores separados por vírgula (CSV)';
$string['csvplus1format'] = 'Arquivo de texto com dados brutos (CSV)';
$string['csvpluspointsformat'] = 'Arquivo de texto com pontos (CSV)';
$string['darkgray'] = 'Cinza escuro';
$string['datanotsaved'] = 'Não foi possível salvar as configurações';
$string['configdecimalplaces'] = 'Número de dígitos que devem ser mostrados após a vírgula decimal ao exibir as notas do questionário off-line.';
$string['decimalplaces'] = 'Casas decimais';
$string['decimalplaces_help'] = 'Número de dígitos que devem ser mostrados após a vírgula decimal ao exibir as notas do questionário off-line.';
$string['deletelistcheck'] = 'Deseja realmente apagar a lista selecionada e todos os participantes?';
$string['deleteresultcheck'] = 'Deseja realmente excluir os resultados selecionados?';
$string['deletepagesafterselected'] = 'Remover quebras de página após perguntas selecionadas';
$string['deletepartcheck'] = 'Deseja realmente excluir os participantes selecionados?';
$string['deletepagecheck'] = 'Deseja realmente excluir as páginas selecionadas?';
$string['deleteparticipantslist'] = 'Excluir lista de participantes';
$string['deletepdfs'] = 'Excluir documentos';
$string['deleteselectedresults'] = 'Excluir resultados selecionados';
$string['deleteselectedpart'] = 'Excluir participantes selecionados';
$string['deletethislist'] = 'Excluir esta lista';
$string['deleteupdatepdf'] = 'Excluir e atualizar formulários PDF';
$string['difficultytitlea'] = 'Dificuldade A';
$string['difficultytitleb'] = 'Dificuldade B';
$string['difficultytitlediff'] = 'Diferença';
$string['difficultytitle'] = 'Dificuldade';
$string['disableimgnewlines'] = 'Desativar novas linhas antes e depois das imagens';
$string['disableimgnewlines_help'] = 'Esta opção desativa novas linhas antes e depois das imagens nas folhas de perguntas em PDF. Aviso: Isso pode causar problemas de formatação. ';
$string['displayoptions'] = 'Opções de exibição';
$string['done'] = 'feito';
$string['downloadallzip'] = 'Baixar todos os arquivos como ZIP';
$string['downloadpartpdf'] = 'Baixar arquivo PDF para a lista \' {$a} \'';
$string['downloadpdfs'] = 'Baixar documentos';
$string['downloadresultsas'] = 'Baixar resultados como:';
$string['dragtoafter'] = 'Depois de {$a}';
$string['dragtostart'] = 'Para o início';
$string['editlist'] = 'Editar lista';
$string['editthislist'] = 'Editar esta lista';
$string['editlists'] = 'Editar listas';
$string['editgroups'] = 'Editar grupos offline';
$string['editgroupquestions'] = 'Editar perguntas do grupo';
$string['editingofflinequiz'] = 'Editando perguntas do grupo';
$string['editingofflinequiz_help'] = 'Ao criar um questionário off-line, os principais conceitos são:
<ul> <li> O teste offline, contendo perguntas em uma ou mais páginas </li>
<li> O banco de perguntas, que armazena cópias de todas as perguntas organizadas em categorias </li> </ul>';
$string['editofflinequiz'] = 'Editar questionário offline';
$string['editingofflinequizx'] = 'Editar questionário offline: {$a}';
$string['editmaxmark'] = 'Editar nota máxima';
$string['editorder'] = 'Editar pedido';
$string['editparticipants'] = 'Editar participantes';
$string['editquestion'] = 'Editar pergunta';
$string['editquestions'] = 'Editar questões';
$string['editscannedform'] = 'Editar formulário digitalizado';
$string['emptygroups'] = 'Alguns grupos de questionários offline estão vazios. Adicione algumas perguntas. ';
$string['enroluser'] = 'Inscrever usuário';
$string['erroraccessingreport'] = 'Você não tem permissão para ver este relatório.';
$string['errorreport'] = 'Relatório de erros de importação';
$string['eventattemptdeleted'] = 'Tentativa de teste offline excluída';
$string['eventattemptpreviewstarted'] = 'A pré-visualização da tentativa do questionário offline foi iniciada';
$string['eventattemptreviewed'] = 'Tentativa de teste offline revisada';
$string['eventattemptsummaryviewed'] = 'Visualização do resumo da tentativa do questionário offline';
$string['eventattemptviewed'] = 'Tentativa de teste offline visualizada';
$string['eventdocscreated'] = 'Formulários de perguntas e respostas de teste offline criados';
$string['eventdocsdeleted'] = 'Formulários de perguntas e respostas do questionário offline excluídos';
$string['eventeditpageviewed'] = 'Visualização da página de edição do questionário offline';
$string['eventofflinequizattemptsubmitted'] = 'Tentativa de teste offline enviada';
$string['eventoverridecreated'] = 'Substituição do questionário offline criada';
$string['eventoverridedeleted'] = 'Substituição do questionário offline excluída';
$string['eventoverrideupdated'] = 'Substituição do questionário offline atualizada';
$string['eventparticipantmarked'] = 'Participante do questionário offline marcado manualmente';
$string['eventquestionmanuallygraded'] = 'Questão avaliada manualmente';
$string['eventreportviewed'] = 'Relatório do questionário offline visualizado';
$string['eventresultsregraded'] = 'Resultados do questionário offline reavaliados';
$string['everythingon'] = 'habilitado';
$string['excelformat'] = 'Folha de cálculo Excel (XLSX)';
$string['experimentalevaluation'] = 'Avaliação experimental das folhas de resposta';
$string['experimentalevaluation_help'] = 'Avaliação experimental das folhas de resposta';
$string['fileprefixanswer'] = 'answer_form';
$string['fileprefixcorrection'] = 'forma_de_correção';
$string['fileprefixform'] = 'question_form';
$string['fileprefixparticipants'] = 'lista de participantes';
$string['fileformat'] = 'Formato para folhas de perguntas';
$string['fileformat_help'] = 'Escolha se deseja suas folhas de perguntas em formato PDF, DOCX ou TEX. Os formulários de resposta e folhas de correção serão sempre gerados em formato PDF. ';
$string['filesizetolarge'] = 'Alguns dos seus arquivos de imagem são muito grandes. As dimensões serão redimensionadas durante a interpretação. Tente digitalizar com uma resolução entre 200 e 300 dpi e no modo preto e branco. Isso irá acelerar a interpretação da próxima vez. ';
$string['filterbytags'] = 'Filtrar por tags ...';
$string['fontsize'] = 'Tamanho da fonte';
$string['forautoanalysis'] = 'Para análise automática';
$string['formforcorrection'] = 'Formulário de correção para o grupo {$a}';
$string['formforgroup'] = 'Formulário de perguntas para o grupo {$a}';
$string['formforgroupdocx'] = 'Formulário de perguntas para o grupo {$a} (DOCX)';
$string['formforgrouplatex'] = 'Formulário de perguntas para o grupo {$a} (LATEX)';
$string['formsexist'] = 'Formulários já criados.';
$string['formsexistx'] = 'Formulários já criados (<a href=(<a href="{$a}"> Baixar formulários </a>)';
$string['formsheetsettings'] = 'Configurações do formulário';
$string['formspreview'] = 'Pré-visualizar para formulários';
$string['formwarning'] = 'Não existe um formulário de resposta definido. Por favor contacte o seu administrador. ';
$string['fromquestionbank'] = 'do banco de questões';
$string['functiondisabledbysecuremode'] = 'Essa funcionalidade está atualmente desabilitada';
$string['generalfeedback'] = 'Feedback geral';
$string['generalfeedback_help'] = 'Feedback geral é o texto mostrado após a tentativa de uma pergunta. Ao contrário do feedback para uma questão específica que depende da resposta dada, o mesmo feedback geral é sempre mostrado. ';
$string['generatepdfform'] = 'Gerar formulário PDF';
$string['grade'] = 'Grau';
$string['gradedon'] = 'Com nota';
$string['gradedscannedform'] = 'Formulário digitalizado com notas';
$string['gradeiszero'] = 'Nota: a nota máxima para este questionário offline é 0 pontos!';
$string['gradeswarning'] = 'As notas das questões devem ser números!';
$string['gradewarning'] = 'A nota da questão deve ser um número!';
$string['gradingofflinequiz'] = 'Notas';
$string['gradingofflinequizx'] = 'Notas: {$a}';
$string['gradingoptionsheading'] = 'Opções de classificação';
$string['greeniscross'] = 'contado como uma cruz';
$string['rediswrong'] = 'cruz errada ou cruz faltando';
$string['group'] = 'Grupo';
$string['groupoutofrange'] = 'Grupo estava fora do intervalo e substituído pelo grupo A.';
$string['groupquestions'] = 'Perguntas do Grupo';
$string['hasresult'] = 'O resultado existe';
$string['hotspotdeletiontask'] = 'Exclusão de pontos de acesso';
$string['html'] = 'HTML';
$string['idnumber'] = 'Número de identificação';
$string['imagefile'] = 'Arquivo de imagem';
$string['imagenotfound'] = 'Arquivo de imagem: {$a} não encontrado!';
$string['imagenotjpg'] = 'Imagem não jpg ou png: {$a}';
$string['imagickwarning'] = 'Imagemagick ausente: Peça ao administrador do sistema para instalar a biblioteca imagemagick e verificar o caminho para o binário convertido nas configurações do filtro de notação TeX. Você não pode importar arquivos TIF sem imagemagick! ';
$string['importerror11'] = 'Existe outro resultado';
$string['importerror12'] = 'Usuário não registrado';
$string['importerror13'] = 'Sem dados de grupo';
$string['importerror14'] = 'Não foi possível agarrar';
$string['importerror15'] = 'Marcações inseguras';
$string['importerror16'] = 'Erro de página';
$string['importerror17'] = 'Páginas incompletas';
$string['importerror21'] = 'Não foi possível agarrar';
$string['importerror22'] = 'Marcações inseguras';
$string['importerror23'] = 'Usuário não está na lista';
$string['importerror24'] = 'Lista não detectada';
$string['importfromto'] = 'Importando {$a->from} para {$a->to} de {$a->total}.';
$string['import'] = 'Importar';
$string['importnew'] = 'Importar';
$string['importnew_help'] = '<p>Você pode importar arquivos de imagem única digitalizados ou vários arquivos de imagem digitalizada em um arquivo ZIP. O módulo de teste offline processará os arquivos de imagem em segundo plano.
Os nomes dos arquivos não são relevantes, mas não devem conter caracteres especiais, como tremas. As imagens devem ser GIFs, PNGs
ou TIFs. Recomenda-se uma resolução entre 200 e 300 dpi.</p>';
$string['importedon'] = 'Importado em';
$string['importforms'] = 'Importar formulários de resposta';
$string['importisfinished'] = 'A importação para o questionário offline {$a} terminou.';
$string['importlinkresults'] = 'Link para resultados: {$a}';
$string['importlinkverify'] = 'Link para verificação: {$a}';
$string['importmailsubject'] = 'notificação de importação de questionário offline';
$string['importnumberexisting'] = 'Número de formas duplas: {$a}';
$string['importnumberpages'] = 'Número de páginas importadas com sucesso: {$a}';
$string['importnumberresults'] = 'Número de importados: {$a}';
$string['importnumberverify'] = 'Número de formulários que precisam ser verificados: {$a}';
$string['importtimefinish'] = 'Processo concluído: {$a}';
$string['importtimestart'] = 'Processo iniciado: {$a}';
$string['inconsistentdata'] = 'Dados inconsistentes: {$a}';
$string['info'] = 'Info';
$string['infoshort'] = 'i';
$string['insecuremarkingsforquestion'] = 'Marcações inseguras precisam de correções manuais para a questão';
$string['insecuremarkings'] = 'Marcações inseguras precisam de correções manuais';
$string['insertnumber'] = 'Por favor insira o número de identificação correto marcado com a moldura azul.';
$string['instruction1'] = 'Este formulário de resposta será verificado automaticamente. Por favor, não dobre ou manche. Use uma caneta preta ou azul para marcar os campos: ';
$string['instruction2'] = 'Apenas marcações claras podem ser interpretadas corretamente! Se você quiser corrigir uma marcação, preencha completamente a caixa com a cor. Este campo será interpretado como uma caixa vazia: ';
$string['instruction3'] = 'Caixas corrigidas não podem ser marcadas novamente. Por favor, não escreva nada fora das caixas. ';
$string['introduction'] = 'Introdução';
$string['invalidformula'] = 'Fórmula inválida para identificação do usuário. A fórmula deve ter o formato <prefixo> [<#digits>] <suffix> = <db-field &>.';
$string['invalidnumberofdigits'] = 'Número inválido de dígitos usados. São permitidos apenas 1 a 9 dígito (s). ';
$string['invaliduserfield'] = 'Campo inválido da tabela de usuário utilizada.';
$string['invigilator'] = 'Invigilator';
$string['ischecked'] = 'Participação marcada';
$string['isnotchecked'] = 'Participação não marcada';
$string['itemdata'] = 'Itemdata';
$string['keepfilesfordays'] = 'Manter arquivos por dias';
$string['letter'] = 'Carta';
$string['lightgray'] = 'Cinza claro';
$string['linktoscannedform'] = 'Ver formulário digitalizado';
$string['listnotdetected'] = 'Não foi possível detectar o código de barras da lista!';
$string['logdeleted'] = 'Entrada de log {$a} excluída.';
$string['convertcommand'] = 'Sintaxe de comando "convert" a ser usado';
$string['logourl'] = 'URL do logotipo';
$string['logourldesc'] = 'URL de um arquivo de imagem que é exibido no canto superior direito dos formulários de resposta, ou seja, <b> http: //www.yoursite.tld/mylogo.png </b> ou <b> ../path/to /your/logo.png </b>. O tamanho máximo permitido é 520x140 pixels. Os formulários de resposta não podem ser avaliados se a imagem exceder o tamanho máximo! ';
$string['lowertrigger'] = 'Segundo limite inferior';
$string['lowertriggerzero'] = 'Segundo limite inferior é zero';
$string['lowerwarning'] = 'Primeiro limite inferior';
$string['lowerwarningzero'] = 'O primeiro limite inferior é zero';
$string['marginwarning'] = 'Por favor, imprima os seguintes arquivos PDF sem margens adicionais! <br /> Evite distribuir fotocópias aos alunos.';
$string['marks'] = 'Marcas';
$string['matrikel'] = 'número do aluno';
$string['maxgradewarning'] = 'A nota máxima tem que ser um número!';
$string['maxmark'] = 'Marca máxima';
$string['membersinplist'] = '{$a->contagem} participantes em  <a href="<a href="{$a->url} "> {$a->nome} </a>';
$string['missingimagefile'] = 'Arquivo de imagem ausente';
$string['missingitemdata'] = 'Resposta (s) ausente (s) para o usuário {$a}';
$string['missinglogdata'] = 'Dados de log ausentes para resultado existente.';
$string['missingquestion'] = 'Esta questão parece não existir mais';
$string['missinguserid'] = 'Falta o número de identificação do usuário! Não foi possível ler o código de barras! ';
$string['modulename_help'] = 'Este módulo permite ao professor criar questionários offline compostos por questões de escolha múltipla'.
$string['moveselectedonpage'] = 'Mover perguntas selecionadas para a página: {$a}';
$string['copyselectedtogroup'] = 'Adicionar perguntas selecionadas ao grupo: {$a}';
$string['copytogroup'] = 'Adicionar todas as questões ao grupo: {$a}';
$string['multichoice'] = 'Múltipla escolha';
$string['multipleanswers'] = 'Escolha pelo menos uma resposta.';
$string['moodleprocessing'] = 'Deixar o Moodle processar dados';
$string['movecorners'] = 'Altere primeiro as posições das marcações dos cantos. Use arrastar e soltar. ';
$string['multianswersforsingle'] = 'Múltiplas respostas para questão de escolha única';
$string['name'] = 'Nome do questionário offline';
$string['neededcorrection'] = '<strong> Atenção: Algumas de suas marcações precisavam de correção manual. Dê uma olhada nos quadrados vermelhos na imagem a seguir.';
$string['newgrade'] = 'Graduado';
$string['newpage'] = 'Nova página';
$string['noattemptexists'] = 'Não existe nenhum resultado';
$string['noattempts'] = 'Nenhum resultado importado!';
$string['noattemptsonly'] = 'Mostrar alunos sem resultados apenas';
$string['nocourse'] = 'O curso com id {$a->curso} ao qual o questionário offline com ID {$a->offlinequiz} pertence está faltando.';
$string['nogradesseelater'] = 'Este questionário ainda não foi avaliado por {$a}. Os resultados serão publicados aqui. ';
$string['nogroupdata'] = 'Sem dados de grupo para o usuário {$a}';
$string['noscannedpage'] = 'Não há página digitalizada com ID {$a}!';
$string['nomcquestions'] = 'Não há questões de múltipla escolha no grupo {$a}!';
$string['noofflinequiz'] = 'Não há teste offline com id {$a}!';
$string['nopages'] = 'Nenhuma página importada';
$string['noparticipantsfound'] = 'Nenhum participante encontrado';
$string['nopdfscreated'] = 'Nenhum documento criado!';
$string['noquestions'] = 'Alguns grupos de questionários offline estão vazios. Adicione algumas perguntas. ';
$string['noquestionsfound'] = 'Não há perguntas no grupo {$a}!';
$string['noquestionsonpage'] = 'Página vazia';
$string['noquestionselected'] = 'Nenhuma pergunta selecionada!';
$string['noresults'] = 'Não há resultados.';
$string['noreview'] = 'Você não tem permissão para revisar este questionário offline';
$string['notagselected'] = 'Nenhuma tag selecionada';
$string['nothingtodo'] = 'Nada a fazer!';
$string['notxtfile'] = 'Nenhum arquivo TXT';
$string['notyetgraded'] = 'Ainda não classificado';
$string['nozipfile'] = 'Nenhum arquivo ZIP';
$string['numattempts'] = 'Número de resultados importados: {$a}';
$string['numattemptsqueue'] = '{$a} formulários de resposta adicionados à fila. Um email será enviado para o seu endereço após o processamento dos dados. ';
$string['numattemptsverify'] = 'Formulários digitalizados aguardando correção: {$a}';
$string['numberformat'] = 'O valor deve ser um número com {$a} dígitos!';
$string['numbergroups'] = 'Número de grupos';
$string['numpages'] = '{$a} páginas importadas';
$string['numquestionsx'] = 'Perguntas: {$a}';
$string['numusersadded'] = '{$a} participantes adicionados';
$string['odsformat'] = 'Planilha OpenDocument (ODS)';
$string['offlineimplementationfor'] = 'Implementação offline para';
$string['editofflinesettings'] = 'Editar configurações offline';
$string['offlinequizcloseson'] = 'A revisão deste questionário offline terminará às {$a}';
$string['offlinequizisclosed'] = 'Questionário offline fechado)';
$string['offlinequizisclosedwillopen'] = 'Teste offline fechado (abre {$a})';
$string['offlinequizisopen'] = 'Este questionário offline está aberto';
$string['offlinequizisopenwillclose'] = 'Questionário offline aberto (fecha {$a})';
$string['offlinequizopenedon'] = 'Este questionário offline começou às {$a}';
$string['offlinequizsettings'] = 'Configurações off-line';
$string['offlinequiz:addinstance'] = 'Adicionar um questionário offline';
$string['offlinequiz:attempt'] = 'Tentar questionários';
$string['offlinequiz:changeevaluationmode'] = 'Possibilita alterar o modo de avaliação. AVISO: NÃO USE EM SISTEMAS DE PRODUÇÃO! ';
$string['offlinequizcloses'] = 'O questionário offline fecha';
$string['offlinequiz:createofflinequiz'] = 'Criar formulários de teste offline';
$string['offlinequiz:deleteattempts'] = 'Excluir resultados do questionário offline';
$string['offlinequiz:grade'] = 'Classifique questionários offline manualmente';
$string['offlinequiz:manage'] = 'Gerenciar questionários offline';
$string['offlinequizopens'] = 'O questionário offline abre';
$string['offlinequiz:preview'] = 'Pré-visualizar questionários offline';
$string['offlinequiz:viewreports'] = 'Ver relatórios de questionários offline';
$string['offlinequiz:view'] = 'Ver informações do questionário offline';
$string['offlinequizwillopen'] = 'O teste offline abre em {$a}';
$string['oneclickenrol'] = 'Inscrição com 1 Clique';
$string['oneclickenroldesc'] = 'Se esta opção estiver ativada, os professores têm a possibilidade de inscrever usuários com um clique enquanto corrigem os formulários de resposta (erro "Usuário não está no curso").';
$string['oneclickrole'] = 'Função para inscrição em 1 clique.';
$string['oneclickroledesc'] = 'Escolha a função usada para inscrição com 1 clique. Apenas funções com o arquétipo "aluno" podem ser selecionadas. ';
$string['onlylocalcategories'] = 'Apenas categorias de questões locais';
$string['orderingofflinequiz'] = 'Pedido e paginação';
$string['orderandpaging'] = 'Pedido e paginação';
$string['orderandpaging_help'] = 'Os números 10, 20, 30, ... opostos a cada questão indicam a ordem das questões. Os números aumentam em etapas de 10 para deixar espaço para perguntas adicionais a serem inseridas. Para reordenar as perguntas, altere os números e clique no botão "Reordenar perguntas".

Para adicionar quebras de página após perguntas específicas, marque as caixas de seleção ao lado das perguntas e clique no botão "Adicionar quebras de página após perguntas selecionadas".

Para organizar as perguntas em várias páginas, clique no botão Repaginar e selecione o número desejado de perguntas por página.';
$string['otherresultexists'] = 'Já existe um resultado diferente para {$a}, importação ignorada! Exclua o resultado primeiro. ';
$string['outof'] = '{$a->grade} de um máximo de {$a->maxgrade}';
$string['outofshort'] = '{$a->grau} / {$a->maxgrade}';
$string['overallfeedback'] = 'Feedback geral';
$string['overview'] = 'Notas';
$string['overviewdownload_help'] = 'Baixar Visão Geral';
$string['pagecorrected'] = 'Folha corrigida da lista de participantes importada';
$string['pageevaluationtask'] = 'Avaliação da folha de resposta para o plug-in offlinequiz';
$string['pageimported'] = 'Folha de lista de participantes importada';
$string['page-mod-offlinequiz-x'] = 'Qualquer página de questionário offline';
$string['page-mod-offlinequiz-edit'] = 'Editar página de teste offline';
$string['pagenumberimported'] = 'Folha {$a} da lista de participantes importada';
$string['pagenumberupdate'] = 'Atualização do número da página';
$string['pagenotdetected'] = 'Não foi possível detectar o código de barras da página!';
$string['pagesizeparts'] = 'Participantes mostrados por página:';
$string['papergray'] = 'Valor branco do papel';
$string['papergray_help'] = 'Se as partes brancas dos formulários de resposta digitalizados estiverem muito escuras, você pode corrigir configurando este valor para cinza escuro.';
$string['partcheckedwithresult'] = '{$a} participantes verificados com resultado';
$string['partcheckedwithoutresult'] = '<a href="{$a->url}"> {$a->count} participantes verificados sem resultado </a>';
$string['partuncheckedwithresult'] = '<a href="{$a->url}"> {$a->count} participantes desmarcados com resultado </a>';
$string['partuncheckedwithoutresult'] = '{$a} participantes desmarcados sem resultado';
$string['partial'] = 'parcial';
$string['participantslist'] = 'Lista de participantes';
$string['participantslists'] = 'Participantes';
$string['participants'] = 'Participantes';
$string['participantsinlists'] = 'Participantes em listas';
$string['participants_help'] = '<p>As listas de participantes são projetadas para grandes questionários offline com muitos participantes. Eles ajudam o professor a verificar quais alunos participaram do questionário e se todos os resultados foram importados corretamente.
Você pode adicionar usuários a listas diferentes. Cada lista pode, por exemplo, conter os participantes de uma determinada sala. Os participantes podem ser membros de um grupo especial. Uma ferramenta de registro de grupo pode ser usada para criar esses grupos.
Listas de participantes podem ser baixadas como documentos PDF, impressas e marcadas com cruzes, assim como os formulários de resposta de questionários off-line. Posteriormente, eles podem ser carregados e os alunos marcados serão marcados como presentes no banco de dados.
Por favor, evite manchas nos códigos de barras, pois eles são usados ​​para identificar os alunos</p>';
$string['partimportnew'] = 'Fazendo upload de listas de participantes';
$string['partimportnew_help'] = '<p>Nesta guia você pode carregar as listas preenchidas de participantes. Você pode fazer upload de arquivos de imagem digitalizados únicos ou vários arquivos de imagem digitalizados em um arquivo ZIP. O módulo de teste offline processará os arquivos de imagem em segundo plano.
Os nomes dos arquivos não são relevantes, mas não devem conter caracteres especiais, como tremas. As imagens devem ser GIFs, PNGs
ou TIFs. Recomenda-se uma resolução entre 200 e 300 dpi.</p>';
$string['pdfdeletedforgroup'] = 'Formulário para grupo {$a} excluído';
$string['pdfscreated'] = 'Formulários PDF foram criados';
$string['pdfsdeletedforgroup'] = 'Formulários do grupo {$a} deletados';
$string['pdfintro'] = 'Informações adicionais';
$string['pdfintro_help'] = 'Esta informação será impressa na primeira página da folha de perguntas e deve conter informações gerais sobre como preencher o formulário de resposta.';
$string['pdfintrotext'] = '<b> Como faço para marcar corretamente? </b> <br /> Este formulário de resposta será verificado automaticamente. Por favor, não dobre ou manche. Use uma caneta preta ou azul para marcar os campos. Se você quiser corrigir uma marcação, preencha completamente a caixa com a cor. Este campo será interpretado como uma caixa vazia. <br /> ';
$string['pdfintrotoolarge'] = 'A introdução é muito longa (máx. 2.000 caracteres).';
$string['pearlywhite'] = 'Branco perolado';
$string['pluginadministration'] = 'Administração offline do questionário';
$string['point'] = 'ponto';
$string['present'] = 'presente';
$string['preventsamequestion'] = 'Impedir o uso múltiplo da mesma pergunta em grupos diferentes';
$string['previewforgroup'] = 'Antevisão para o grupo {$a}';
$string['username'] = 'Usuário';
$string['preview'] = 'Pré-visualizar';
$string['previewquestion'] = 'Pré-visualizar pergunta';
$string['printstudycodefield'] = 'Imprimir campo do código do estudo na folha de perguntas';
$string['printstudycodefield_help'] = 'Se assinalado, o campo do código do estudo será impresso na primeira página da ficha de perguntas.';
$string['privacy:data_folder_name'] = 'Dados do questionário offline';
$string['privacy:metadata:core_files'] = 'O offlinequiz usa o arquivo API para armazenar as folhas de perguntas geradas, folhas de respostas e folhas de correção, bem como as folhas de respostas preenchidas.';
$string['privacy:metadata:core_question'] = 'O offlinequiz usa a API de perguntas para salvar as perguntas dos questionários.';
$string['privacy:metadata:mod_quiz'] = 'O offlinequiz usa a API do questionário para salvar os resultados dos questionários.';
$string['privacy:metadata:offlinequiz:course'] = 'A coluna \' curso \' na tabela do questionário offline salva em qual curso este questionário offline está armazenado.';
$string['privacy:metadata:offlinequiz:name'] = 'A coluna \' nome \' salva o nome do questionário offline.';
$string['privacy:metadata:offlinequiz:introformat'] = 'Este campo não é usado.';
$string['privacy:metadata:offlinequiz:pdfintro'] = 'As informações adicionais que são inseridas nas folhas de perguntas no início.';
$string['privacy:metadata:offlinequiz:timeopen'] = 'A coluna timeopen salva quando um questionário offline foi / será aberto.';
$string['privacy:metadata:offlinequiz:timeclose'] = 'A coluna timeclose salva quando o offlinequiz foi / será fechado.';
$string['privacy:metadata:offlinequiz:time'] = 'A coluna de tempo salva a data do questionário offline.';
$string['privacy:metadata:offlinequiz:grade'] = 'A nota mostra a quantidade máxima de pontos para obter neste questionário offline.';
$string['privacy:metadata:offlinequiz:numgroups'] = 'A quantidade de grupos que este questionário offline possui.';
$string['privacy:metadata:offlinequiz:decimalpoints'] = 'A quantidade de casas decimais a calcular para as notas.';
$string['privacy:metadata:offlinequiz:review'] = 'Esta coluna salva informações sobre como a revisão é feita.';
$string['privacy:metadata:offlinequiz:docscreated'] = 'Se os documentos foram criados, este campo é definido como 1, caso contrário é 0.';
$string['privacy:metadata:offlinequiz:shufflequestions'] = 'Uma preferência se as perguntas devem ser embaralhadas ao criar um questionário offline; 1 para embaralhar, 0 caso contrário. ';
$string['privacy:metadata:offlinequiz:printstudycodefield'] = 'Uma preferência se o código do estudo deve ser impresso no formulário da pergunta. 1 para verdadeiro, 0 caso contrário. ';
$string['privacy:metadata:offlinequiz:fontsize'] = 'O tamanho da fonte nas fichas de perguntas.';
$string['privacy:metadata:offlinequiz:timecreated'] = 'A coluna criada com o tempo salva quando o questionário offline foi criado.';
$string['privacy:metadata:offlinequiz:timemodified'] = 'A coluna timemodified economiza o tempo quando o offlinequiz foi alterado pela última vez.';
$string['privacy:metadata:offlinequiz:fileformat'] = 'O formato de arquivo que é usado para imprimir as folhas de perguntas, 0 para pdf, 1 para docx, 2 para LaTeX.';
$string['privacy:metadata:offlinequiz:showquestioninfo'] = 'Salva se uma informação sobre as questões deve ser exibida, 0 para não, 1 para informações sobre o tipo de pergunta, 2 para informações sobre a quantidade de respostas certas.';
$string['privacy:metadata:offlinequiz:showgrades'] = 'Salva se a quantidade de pontos a obter com a questão deve ser impressa na folha de questões.';
$string['privacy:metadata:offlinequiz:showtutorial'] = 'Salva se os participantes devem ser convidados a fazer um tutorial offline do questionário.';
$string['privacy:metadata:offlinequiz:id_digits'] = 'Salva a quantidade de dígitos que o número de id tinha quando as folhas de resposta foram criadas. Isso é necessário para compatibilidade retroativa se a quantia for levantada entre a criação e a importação das folhas de respostas. ';
$string['privacy:metadata:offlinequiz:disableimgnewlines'] = 'Deve desabilitar novas linhas antes e depois das imagens.';
$string['privacy:metadata:offlinequiz'] = 'A tabela offlinequiz salva todas as informações específicas de uma instância offlinequiz.';
$string['privacy:metadata:offlinequiz_choices:scannedpageid'] = 'A página digitalizada à qual a escolha se refere.';
$string['privacy:metadata:offlinequiz_choices:slotnumber'] = 'O espaço de perguntas desta escolha.';
$string['privacy:metadata:offlinequiz_choices:choicenumber'] = 'O número da escolha para esta questão.';
$string['privacy:metadata:offlinequiz_choices:value'] = 'É a escolha considerada riscada. 0 para não, 1 para sim, -1 para incerto. ';
$string['privacy:metadata:offlinequiz_choices'] = 'Esta tabela contém as informações de todas as cruzes de todas as páginas digitalizadas. A informação é necessária para posteriormente criar resultados com base nos cruzamentos. ';
$string['privacy:metadata:offlinequiz_group_questions:offlinequizid'] = 'O questionário offline ao qual este grupo se refere.';
$string['privacy:metadata:offlinequiz_group_questions:offlinegroupid'] = 'O grupo de perguntas off-line ao qual esta pergunta se refere.';
$string['privacy:metadata:offlinequiz_group_questions:questionid'] = 'O id da questão selecionada.';
$string['privacy:metadata:offlinequiz_group_questions:position'] = 'A posição neste questionário offline.';
$string['privacy:metadata:offlinequiz_group_questions:page'] = 'A página onde esta questão é impressa nas folhas de resposta.';
$string['privacy:metadata:offlinequiz_group_questions:slot'] = 'O espaço da pergunta no questionário.';
$string['privacy:metadata:offlinequiz_group_questions:maxmark'] = 'A quantidade máxima de pontos que podem ser alcançados com esta questão.';
$string['privacy:metadata:offlinequiz_group_questions'] = 'Esta tabela salva todas as questões para todos os grupos de questionários offline.';
$string['privacy:metadata:offlinequiz_groups:offlinequizid'] = 'O id do offlinequiz ao qual este offlinequiz pertence.';
$string['privacy:metadata:offlinequiz_groups:number'] = 'O número do grupo para este questionário offline, 1 para o grupo A, 2 para o grupo B e assim por diante.';
$string['privacy:metadata:offlinequiz_groups:sumgrades'] = 'A soma de todas as notas de todas as questões deste grupo.';
$string['privacy:metadata:offlinequiz_groups:numberofpages'] = 'A quantidade de páginas necessárias para imprimir as folhas de resposta.';
$string['privacy:metadata:offlinequiz_groups:templateusageid'] = 'O id de uso do modelo, que é usado para criar um resultado na API do quiz.';
$string['privacy:metadata:offlinequiz_groups:questionfilename'] = 'O nome do arquivo que foi usado para salvar o arquivo da pergunta';
$string['privacy:metadata:offlinequiz_groups:answerfilename'] = 'O nome do arquivo que foi usado para salvar o arquivo de resposta.';
$string['privacy:metadata:offlinequiz_groups:correctionfilename'] = 'O arquivo que foi usado para salvar o arquivo de correção.';
$string['privacy:metadata:offlinequiz_groups'] = 'Mesa dos grupos em que participam os questionários offline.';
$string['privacy:metadata:offlinequiz_hotspots:scannedpageid'] = 'Página digitalizada na qual o ponto de acesso está.';
$string['privacy:metadata:offlinequiz_hotspots:name'] = 'Tipo de ponto de acesso, por exemplo, u% número para ponto de acesso do usuário, a-0-0 para pergunta 1, resposta 1, e assim por diante.';
$string['privacy:metadata:offlinequiz_hotspots:x'] = 'O valor x do ponto de acesso.';
$string['privacy:metadata:offlinequiz_hotspots:y'] = 'O valor y do ponto de acesso.';
$string['privacy:metadata:offlinequiz_hotspots:blank'] = 'Se o ponto de acesso for analisado com sucesso.';
$string['privacy:metadata:offlinequiz_hotspots:time'] = 'A hora da última atualização para este hotspot.';
$string['privacy:metadata:offlinequiz_hotspots'] = 'Esta tabela salva todas as posições das caixas e se elas são avaliadas com sucesso.';
$string['privacy:metadata:offlinequiz:papergray'] = 'O valor de branco deste questionário offline.';
$string['privacy:metadata:offlinequiz_page_corners:scannedpageid'] = 'A página digitalizada neste canto está.';
$string['privacy:metadata:offlinequiz_page_corners:x'] = 'O valor x do canto.';
$string['privacy:metadata:offlinequiz_page_corners:y'] = 'O valor y do canto.';
$string['privacy:metadata:offlinequiz_page_corners:position'] = 'A informação se este canto está na parte superior ou inferior e direita ou esquerda.';
$string['privacy:metadata:offlinequiz_page_corners'] = 'Esta tabela salva todos os cantos de cada página digitalizada para avaliá-la mais rapidamente para a próxima avaliação ou correção.';
$string['privacy:metadata:offlinequiz_participants:listid'] = 'O id da lista em que este participante está.';
$string['privacy:metadata:offlinequiz_participants:userid'] = 'O ID do usuário do usuário.';
$string['privacy:metadata:offlinequiz_participants:checked'] = 'A informação se este usuário foi verificado na lista de participantes.';
$string['privacy:metadata:offlinequiz_participants'] = 'A tabela de participantes salva se o usuário estava participando do questionário offline ou não.';
$string['privacy:metadata:offlinequiz_p_choices:scannedpageid'] = 'A página digitalizada à qual esta escolha está relacionada.';
$string['privacy:metadata:offlinequiz_p_choices:userid'] = 'O ID do usuário ao qual esta escolha está relacionada.';
$string['privacy:metadata:offlinequiz_p_choices:value'] = 'Se a cruz for preenchida ou não (0 para não preenchida, 1 para preenchida, -1 para insegura).';
$string['privacy:metadata:offlinequiz_p_choices'] = 'Esta tabela guarda todos os cruzamentos para as listas de participantes.';
$string['privacy:metadata:offlinequiz_p_lists:offlinequizid'] = 'O questionário offline ao qual esta lista pertence.';
$string['privacy:metadata:offlinequiz_p_lists:name'] = 'O nome da lista de participantes.';
$string['privacy:metadata:offlinequiz_p_lists:number'] = 'O número da lista no questionário offline.';
$string['privacy:metadata:offlinequiz_p_lists:filename'] = 'O nome do arquivo para a lista.';
$string['privacy:metadata:offlinequiz_p_lists'] = 'Esta tabela guarda informação sobre as listas de participantes onde os professores podem riscar, se um aluno esteve presente ou não.';
$string['privacy:metadata:offlinequiz_queue'] = 'Esta tabela salva uma fila de upload. Para cada upload existe um objeto próprio nesta tabela. ';
$string['privacy:metadata:offlinequiz_queue:offlinequizid'] = 'O id offlinequiz da fila.';
$string['privacy:metadata:offlinequiz_queue:importuserid'] = 'O userid do professor que importou os arquivos.';
$string['privacy:metadata:offlinequiz_queue:timecreated'] = 'A hora em que estas folhas de questionário offline foram importadas.';
$string['privacy:metadata:offlinequiz_queue:timestart'] = 'A hora em que a avaliação da fila foi iniciada.';
$string['privacy:metadata:offlinequiz_queue:timefinish'] = 'A hora em que a avaliação da fila foi finalizada.';
$string['privacy:metadata:offlinequiz_queue:status'] = 'O estado da fila necessária.';
$string['privacy:metadata:offlinequiz_queue_data:queueid'] = 'A fila à qual estes dados pertencem.';
$string['privacy:metadata:offlinequiz_queue_data:filename'] = 'O nome do arquivo ao qual esta fila está relacionada.';
$string['privacy:metadata:offlinequiz_queue_data:status'] = 'O estado dos dados da fila.';
$string['privacy:metadata:offlinequiz_queue_data:error'] = 'Se o status for erro, aqui ficará uma mensagem de erro mais detalhada.';
$string['privacy:metadata:offlinequiz_queue_data'] = 'Esta tabela salva dados para a fila, pois cada arquivo na fila receberá um objeto de dados de fila.';
$string['privacy:metadata:offlinequiz_results:offlinequizid'] = 'O questionário offline, ao qual este resultado pertence.';
$string['privacy:metadata:offlinequiz_results:offlinegroupid'] = 'O grupo offlinequiz ao qual este resultado pertence.';
$string['privacy:metadata:offlinequiz_results:userid'] = 'O usuário ao qual este resultado pertence.';
$string['privacy:metadata:offlinequiz_results:sumgrades'] = 'A soma de todas as notas para este resultado.';
$string['privacy:metadata:offlinequiz_results:usageid'] = 'O ID de uso do modelo da API do questionário onde este resultado é salvo.';
$string['privacy:metadata:offlinequiz_results:teacherid'] = 'O professor que enviou o resultado.';
$string['privacy:metadata:offlinequiz_results:status'] = 'O estado do resultado (incompleto ou completo).';
$string['privacy:metadata:offlinequiz_results:timestart'] = 'O início da hora em que o resultado foi inserido pela primeira vez.';
$string['privacy:metadata:offlinequiz_results:timefinish'] = 'A hora de término em que o resultado foi inserido pela primeira vez.';
$string['privacy:metadata:offlinequiz_results:timemodified'] = 'A data de modificação do resultado.';
$string['privacy:metadata:offlinequiz_results'] = 'Esta tabela salva todos os dados do resultado, que não podem ser armazenados na API do questionário.';
$string['privacy:metadata:offlinequiz_scanned_pages:offlinequizid'] = 'O questionário offline da página verificada.';
$string['privacy:metadata:offlinequiz_scanned_pages:resultid'] = 'O resultado relativo a esta página.';
$string['privacy:metadata:offlinequiz_scanned_pages:filename'] = 'O nome do arquivo da página escaneada.';
$string['privacy:metadata:offlinequiz_scanned_pages:warningfilename'] = 'O nome do arquivo que é criado quando um questionário preenchido incorretamente é corrigido e o usuário recebe um aviso sobre isso.';
$string['privacy:metadata:offlinequiz_scanned_pages:groupnumber'] = 'O número do grupo ao qual este questionário offline pertence.';
$string['privacy:metadata:offlinequiz_scanned_pages:userkey'] = 'O userkey (não o userid) do usuário riscado na página.';
$string['privacy:metadata:offlinequiz_scanned_pages:pagenumber'] = 'O número da página desta página.';
$string['privacy:metadata:offlinequiz_scanned_pages:time'] = 'A hora em que a página foi processada.';
$string['privacy:metadata:offlinequiz_scanned_pages:status'] = 'O status desta página.';
$string['privacy:metadata:offlinequiz_scanned_pages:error'] = 'O erro detalhado desta página (se existir).';
$string['privacy:metadata:offlinequiz_scanned_pages'] = 'A tabela salva informações sobre uma página digitalizada de um questionário offline.';
$string['privacy:metadata:offlinequiz_scanned_p_pages:offlinequizid'] = 'O questionário offline ao qual esta página de participantes pertence.';
$string['privacy:metadata:offlinequiz_scanned_p_pages:listnumber'] = 'O número da lista.';
$string['privacy:metadata:offlinequiz_scanned_p_pages:filename'] = 'O nome do arquivo para a página escaneada.';
$string['privacy:metadata:offlinequiz_scanned_p_pages:time'] = 'A hora em que esta página foi processada.';
$string['privacy:metadata:offlinequiz_scanned_p_pages:status'] = 'O status desta página verificada.';
$string['privacy:metadata:offlinequiz_scanned_p_pages:error'] = 'O erro (se existir) que esta página disparou durante o processamento.';
$string['privacy:metadata:offlinequiz_scanned_p_pages'] = 'Esta tabela salva as páginas dos participantes e suas informações gerais.';
$string['questionanalysis'] = 'Análise de dificuldade';
$string['questionanalysistitle'] = 'Tabela de análise de dificuldade';
$string['questionbankcontents'] = 'Conteúdo do banco de perguntas';
$string['questionforms'] = 'Formulários de perguntas';
$string['questioninfoanswers'] = 'Número de respostas corretas';
$string['questioninfocorrectanswer'] = 'resposta correta';
$string['questioninfocorrectanswers'] = 'respostas corretas';
$string['questioninfonone'] = 'Nada';
$string['questioninfoqtype'] = 'Tipo de pergunta';
$string['questionname'] = 'Nome da pergunta';
$string['questionpage'] = 'Página';
$string['questionsheet'] = 'Folha de perguntas';
$string['questionsheetlatextemplate'] = '% !TEX encoding = UTF-8 Unicode
\documentclass[11pt,a4paper]{article}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\textwidth 16truecm
\textheight 23truecm
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\topmargin}{-1cm}
\usepackage{amsmath} % for \implies etc
\usepackage{amsfonts} % for \mathbb etc
\usepackage[colorlinks=true,urlcolor=dunkelrot,linkcolor=black]{hyperref} % Para usar hyperlinks
\usepackage{ifthen}
\usepackage{enumitem}
\usepackage{xcolor}
\usepackage{ulem}
\parindent 0pt % Sem indentação no início de uma seção
\renewcommand\UrlFont{\sf}
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\chead{\sc \Title, Group \Group}
\cfoot{Seite \thepage/\pageref{LastPage}}
\makeatletter %%% desabilitar quebra de página entre respostas
\@beginparpenalty=10000
\@itempenalty=10000
\makeatother
%
\newcommand{\answerIs}[1]{} %%%Desabilitar exibição de resposta correta
% \newcommand{\answerIs}[1]{[#1]} %%%Habilitar exibição de resposta correta
%%%


% ===========================================================================================================
%%% Course data:
\newcommand{\Group}{{$a->groupname}}
\newcommand{\Title}{{$a->coursename}}
\newcommand{\Date}

\newcommand{\TestTitle}{%
\begin{center}
{\bf \Large Question sheet}\\\\[3mm]
\fbox{
\begin{tabular}{rl}
\rule{0pt}{25pt} Name: & $\underline{\hspace*{8cm}}$ \rule{20pt}{0pt}\\\\[5mm]
ID number: & $\underline{\hspace*{8cm}}$\\\\[5mm]
\ifthenelse{\equal{true}{{$a->printstudycodefield}}}{\rule{10pt}{0pt} Study code: & $\underline{\hspace*{8cm}}$\\\\[5mm]}{}
\rule[-20pt]{0pt}{20pt} Signature: & $\underline{\hspace*{8cm}}$
\end{tabular}}
\end{center}
}

\InputIfFileExists{offline_test_extras.tex}{}{} % Definições extras de usuário

\begin{document}


% ===========================================================================================================
\TestTitle

% ===========================================================================================================


\bigskip
% ===========================================================================================================

{$a->pdfintrotext}

% ===========================================================================================================

\newpage

% ===========================================================================================================


{$a->latexforquestions}


\end{document}';
$string['questionsin'] = 'Perguntas em';
$string['questionsingroup'] = 'Perguntas em grupo';
$string['questionsinthisofflinequiz'] = 'Perguntas neste questionário offline';
$string['questiontextisempty'] = '[Texto da pergunta em branco]';
$string['quizdate'] = 'Data do questionário offline';
$string['quizopenclose'] = 'Datas de abertura e fechamento';
$string['quizopenclose_help'] = 'Os alunos só podem ver as suas tentativas após a hora de abertura e antes da hora de fecho.';
$string['quizquestions'] = 'Perguntas do questionário';
$string['randomfromexistingcategory'] = 'Questão aleatória de uma categoria existente';
$string['randomnumber'] = 'Número de questões aleatórias';
$string['randomquestionusinganewcategory'] = 'Questão aleatória usando uma nova categoria';
$string['readjust'] = 'Reajustar';
$string['reallydeletepdfs'] = 'Deseja realmente deletar os arquivos do formulário?';
$string['reallydeleteupdatepdf'] = 'Deseja realmente excluir e atualizar a lista de participantes?';
$string['recreatepdfs'] = 'Recriar PDFs';
$string['recurse'] = 'Incluir questões de subcategorias também';
$string['refreshpreview'] = 'Atualizar visualização';
$string['regrade'] = 'Regrade';
$string['regradedisplayexplanation'] = '<b> Atenção: </b> A reclassificação não irá alterar as marcas que foram substituídas manualmente!';
$string['regradinginfo'] = 'Se alterar a pontuação de uma questão, deve reagrupar o questionário offline para atualizar os resultados dos participantes.';
$string['regradingquiz'] = 'Regrading';
$string['regradingresult'] = 'Novo resultado para o usuário {$a} ...';
$string['reloadpreview'] = 'Atualizar visualização';
$string['reloadquestionlist'] = 'Recarregar lista de perguntas';
$string['remove'] = 'Remover';
$string['removeemptypage'] = 'Remover página vazia';
$string['removepagebreak'] = 'Remover quebra de página';
$string['removeselected'] = 'Remover selecionados';
$string['reordergroupquestions'] = 'Reordenar perguntas do grupo';
$string['reorderquestions'] = 'Reordenar perguntas';
$string['reordertool'] = 'Mostrar ferramenta de reordenamento';
$string['repaginate'] = 'Repaginar com {$a} perguntas por página';
$string['repaginatecommand'] = 'Repaginar';
$string['repaginatenow'] = 'Repaginar agora';
$string['reportends'] = 'Fim da revisão dos resultados';
$string['reportoverview'] = 'Visão geral';
$string['reportstarts'] = 'A revisão dos resultados começa';
$string['resetofflinequizzes'] = 'Redefinir dados do questionário offline';
$string['results'] = 'Resultados';
$string['resultexists'] = 'Mesmo resultado para {$a} já existe, importação ignorada';
$string['resultimport'] = 'Importar resultados';
$string['reviewcloses'] = 'A revisão fecha';
$string['reviewbefore'] = 'Permitir revisão enquanto o questionário offline estiver aberto';
$string['reviewclosed'] = 'Após o encerramento do questionário offline';
$string['reviewimmediately'] = 'Imediatamente após a tentativa';
$string['reviewincludes'] = 'Revisão inclui';
$string['reviewofresult'] = 'Revisão do resultado';
$string['reviewopens'] = 'Revisão abre';
$string['reviewoptions'] = 'Os alunos podem ver';
$string['reviewoptionsheading'] = 'Opções de revisão';
$string['reviewoptions_help'] = 'With these options you can control what the students may see after the results were imported.
You can also define start and end time for the results report. The checkboxes mean:
<table>
<tr><td style="vertical-align: top;"><b>The attempt</b></td><td>
The text of the questions and answers will be shown to the students. They will see which answers they chose, but the correct answers will not be indicated.</td>
</td></tr>
<tr><td style="vertical-align: top;"><b>Whether correct</b></td><td>
This option can only be activated if the option "The attempt" is activated. If activated, the students can see which of the chosen answers are correct (green background) or incorrect (red background).
</td></tr>
<tr><td style="vertical-align: top;"><b>Marks</b></td><td>
The group (e.g. B), scores (achieved grade, total grade for questions, achieved in percent, e.g. 40/80 (50)) and the grade (e.g. 50 out of a maximum of 100) are displayed.
Additionally, if "The attempt" is selected, the achieved score and the maximum score are shown for every question.
</td></tr>
<tr><td style="vertical-align: top;"><b>Specific feedback</b></td><td>
Feedback that depens on what response the student gave.
</td></tr>
<tr><td style="vertical-align: top;"><b>General feedback</b></td><td>
<p>General feedback is shown to the student after the results were imported.
Unlike specific feedback, which depends on the question type and what response the student gave, the same general feedback text is shown to all students.</p>
<p></p><p>You can use the general feedback to give students a fully worked answer and perhaps a link to more information they can use if they did not understand the questions.</p>
</td></tr>
<tr><td style="vertical-align: top;"><b>Right Answers</b></td><td>
It is shown which answers are correct or wrong. This option is only available if "The attempt" is set.
</td></tr>
<tr><td style="vertical-align: top;"><b>Scanned form</b></td><td>
The scanned answer forms are shown. Checked boxes are marked with green squares.
</td></tr>
<tr><td style="vertical-align: top;"><b>Scanned form with grades</b></td><td>
The scanned answer forms are shown. Checked boxes are marked with green squares. Wrong marks and missing marks are highlighted.
Additionally, a table shows the maximum grade and the achieved grade for every question.
</td></tr>
</table>';
$string['review'] = 'Revisão';
$string['rimport'] = 'Múltipla-escolha';
$string['rotate'] = 'Girar';
$string['rotatingsheet'] = 'Folha é girada ...';
$string['saveandshow'] = 'Salvar e mostrar alterações para o aluno';
$string['save'] = 'Salvar';
$string['savescannersettings'] = 'Salvar configurações do scanner';
$string['scannedform'] = 'Formulário digitalizado';
$string['scannerformfortype'] = 'Formulário para tipo {$a}';
$string['scanningoptionsheading'] = 'Opções de digitalização';
$string['scanneroptions'] = 'Configurações do scanner';
$string['scannerpdfs'] = 'Formulários vazios';
$string['scannerpdfstext'] = 'Baixe os seguintes formulários vazios se desejar usar seu próprio software de scanner.';
$string['score'] = 'Pontuação';
$string['search:activity'] = 'Teste offline - informações de atividade';
$string['select'] = 'Selecionar';
$string['selectagroup'] = 'Selecione um grupo';
$string['selectall'] = 'Selecionar tudo';
$string['selectcategory'] = 'Selecionar categoria';
$string['selectedattempts'] = 'Tentativas selecionadas ...';
$string['selectnone'] = 'Desmarcar tudo';
$string['selectquestiontype'] = '- Selecione o tipo de pergunta -';
$string['selectdifferentgroup'] = 'Selecione um grupo diferente!';
$string['selectformat'] = 'Selecionar formato ...';
$string['selectgroup'] = 'Selecionar grupo';
$string['selectlist'] = 'Selecione uma lista ou tente reajustar a planilha:';
$string['selectmultipletoolbar'] = 'Selecionar barra de ferramentas múltipla';
$string['selectpage'] = 'Selecione o número da página ou tente reajustar a folha:';
$string['showallparts'] = 'Mostrar todos os {$a} participantes';
$string['showcopyright'] = 'Mostrar declaração de direitos autorais';
$string['showcopyrightdesc'] = 'Se você ativar esta opção, uma declaração de direitos autorais será mostrada na página de revisão de resultados do aluno.';
$string['showgrades'] = 'Imprimir notas das questões';
$string['showgrades_help'] = 'Esta opção controla se as notas máximas das perguntas do questionário offline devem ser impressas na folha de perguntas.';
$string['showmissingattemptonly'] = 'Mostrar todos os participantes marcados sem resultado';
$string['showmissingcheckonly'] = 'Mostrar todos os participantes desmarcados com resultado';
$string['shownumpartsperpage'] = 'Mostrar {$a} participantes por página';
$string['showquestioninfo'] = 'Imprimir informações sobre as respostas';
$string['showquestioninfo_help'] = 'Com esta opção, você pode controlar quais informações adicionais sobre a pergunta são impressas na folha de perguntas.
Você pode escolher um destes:
<ul>
<li> Nada
<li> Tipo de pergunta - Dependendo do tipo de pergunta, escolha única, múltipla escolha, tudo ou nada, a múltipla escolha será impressa
<li> Número de respostas corretas - O número de respostas corretas será impresso
</ul>';
$string['showstudentview'] = 'Mostrar visão do aluno';
$string['showtutorial'] = 'Mostrar um tutorial de questionário offline para os alunos.';
$string['showtutorial_help'] = 'Esta opção determina se os alunos podem ver um tutorial sobre os conceitos básicos de questionários offline.
O tutorial fornece informações sobre como lidar com os diferentes tipos de documentos em questionários offline. Em uma parte interativa, eles aprendem como marcar sua carteira de estudante corretamente. <br />
<b> Observação: </b> <br />
Se você definir esta opção como "Sim", mas ocultar o questionário off-line, o link para o tutorial não ficará visível. Nesse caso, você pode adicionar um link para o tutorial na página do curso.';
$string['showtutorialdescription'] = 'Você pode adicionar um link do tutorial para a página do curso usando a seguinte URL:';
$string['shuffleanswers'] = 'Respostas aleatórias';
$string['shufflequestions'] = 'Perguntas aleatórias';
$string['shufflequestionsanswers'] = 'Misturar perguntas e respostas';
$string['shufflequestionsselected'] = 'As perguntas aleatórias foram definidas, portanto, algumas ações relacionadas às páginas não estão disponíveis. Para alterar a opção de ordem aleatória, {$a}. ';
$string['shufflewithin'] = 'Ordem aleatória nas perguntas';
$string['shufflewithin_help'] = 'Se habilitado, as partes que compõem cada questão serão aleatoriamente embaralhadas cada vez que você pressionar o botão recarregar na visualização do formulário. NOTA: Esta configuração só se aplica a questões que tenham a opção de embaralhamento ativada. ';
$string['signature'] = 'Assinatura';
$string['singlechoice'] = 'Escolha única';
$string['standard'] = 'Padrão';
$string['starttutorial'] = 'Iniciar tutorial sobre o exame';
$string['statistics'] = 'Estatísticas';
$string['statisticsplural'] = 'Estatísticas';
$string['statsoverview'] = 'Visão geral das estatísticas';
$string['studycode'] = 'Código de estudo';
$string['temporaryfiledeletiontask'] = 'Excluir arquivos temporários';
$string['theattempt'] = 'A tentativa';
$string['timesup'] = 'O tempo acabou!';
$string['totalmarksx'] = 'Total de notas: {$a}';
$string['totalpointsx'] = 'Total de notas: {$a}';
$string['totalquestionsinrandomqcategory'] = 'Total de {$a} questões na categoria.';
$string['trigger'] = 'limite inferior / superior';
$string['tutorial'] = 'Tutorial para questionários offline';
$string['type'] = 'Tipo';
$string['uncheckparts'] = 'Marcar participantes selecionados como ausentes';
$string['updatedsumgrades'] = 'A soma de todas as notas do grupo {$a->letra} foi recalculada para {$a->nota}.';
$string['upgradingfilenames'] = 'Atualizando nomes de arquivos de documentos: questionário offline {$a->done} / {$a->outof} (ID do questionário offline {$a->info})';
$string['upgradingofflinequizattempts'] = 'Atualizando tentativas de teste offline: teste offline {$a->done} / {$a->outof} <br/> (ID do questionário offline {$a->info})';
$string['upgradingilogs'] = 'Atualizando páginas escaneadas: página escaneada {$a->done} / {$a->outof} <br/> (ID do questionário off-line {$a->info})';
$string['uploadpart'] = 'Carregar / Corrigir listas de participantes';
$string['upload'] = 'Carregar / Corrigir M.Es';
$string['uppertrigger'] = 'Segundo limite superior';
$string['uppertriggerzero'] = 'O segundo limite superior é zero';
$string['upperwarning'] = 'Primeiro limite superior';
$string['upperwarningzero'] = 'O primeiro limite superior é zero';
$string['useradded'] = 'Usuário {$a} adicionado';
$string['userdoesnotexist'] = 'O usuário {$a} não existe no sistema';
$string['useridentification'] = 'Identificação do usuário';
$string['useridviolation'] = 'Vários usuários encontrados';
$string['userimported'] = 'Usuário {$a} importado e avaliado';
$string['usernotincourse'] = 'Usuário {$a} não está no curso.';
$string['userpageimported'] = 'Única página importada para o usuário {$a}';
$string['usernotinlist'] = 'Usuário não registrado na lista!';
$string['usernotregistered'] = 'Usuário {$a} não registrado no curso';
$string['valuezero'] = 'O valor não deve ser zero';
$string['viewresults'] = 'Ver resultados';
$string['white'] = 'Branco';
$string['withselected'] = 'Com selecionado ...';
$string['zipfile'] = 'Arquivo ZIP';
$string['zipok'] = 'Arquivo ZIP importado';
$string['zerogradewarning'] = 'Aviso: sua nota no questionário off-line é 0,0!';
$string['ID_prefix'] = 'Prefixo do número do aluno';
$string['ID_postfix'] = 'Sufixo do número do aluno';
$string['empty_ignored_submit'] = 'Não há notas para avaliar. Isso é normal se você optou pelo modo "Ignorar" notas.';